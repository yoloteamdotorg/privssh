var mongoose = require('mongoose')

var CampaignSchema = mongoose.Schema({
  campaignName: {
    type: String,
    unique: true,
    required: true,
    lowercase: true
  },
  unlimited: {
    type: Boolean,
    required: true
  },
  autoRenew: Boolean,
  country: [String],
  socks: [mongoose.Schema.Types.Mixed],
  usedSocks: [String],
  totalSocks: Number
}, { versionKey: false })

module.exports = mongoose.model('Campaigns', CampaignSchema, 'Campaigns')
