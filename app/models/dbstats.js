var mongoose = require('mongoose')

var DbSchema = mongoose.Schema({
  rootStats: {
    type: Boolean,
    required: true,
    unique: true,
    dropDups: true,
    default: true
  },
  uploaded: {
    type: Number,
    required: true,
    min: 0,
    default: 0
  },
  used: {
    type: Number,
    required: true,
    min: 0,
    default: 0
  },
  lastUpdate: String
}, { versionKey: false })

module.exports = mongoose.model('DbStats', DbSchema, 'DbStats')
