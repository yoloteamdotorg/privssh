PrivateSsh v2.2
+ Thay đổi lại giao diện Upload
+ Cải thiện tốc độ trang Admin
+ Hỗ trợ Auto Upload (từ phiên bản 2.1)
+ Campaign giờ có 2 loại: Limited (Import số Socks cố định) và Unlimited (Loại này Upload xong tự cập nhật vào Campaign)
+ Định dạng Socks hỗ trợ dạng không có user hoặc không có password

Hướng dẫn cài đặt trên VPS Linux - HĐH CentOS

===========================================

Bước 1: Cài đặt NodeJS và các công cụ cần thiết, đã bao gồm cập nhật HĐH - http://nodebb.readthedocs.io/en/latest/installing/os/centos.html

`yum -y update && yum -y install epel-release && yum -y groupinstall "Development Tools" && yum -y install git net-tools && curl --silent --location https://rpm.nodesource.com/setup_7.x | bash - && yum -y install nodejs`


===========================================

Bước 2: Cài đặt MongoDB để quản lý dữ liệu - https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-centos-7

```
vi /etc/yum.repos.d/mongodb-org.repo

[mongodb-org-3.4]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.4/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc

yum repolist
yum -y install mongodb-org && systemctl start mongod
```



===========================================

Bước 3: Thiết lập dữ liệu mặc định cho Web

```
mongo
use YoloTEAM-PrivateSshServer
db.Users.insert({username: "admin", password: "$2a$10$BmjjZnr8HScvlXDqhzqaGe9UkJ6MRYgxytw4EfqPqMYCV1bKA/dzK", isAdmin: true})
db.DbStats.insert({rootStats: true, uploaded: 0, used: 0})
```


username: admin / password: admin

===========================================

Bước 4: Cài đặt và khởi chạy PrivateSshManager

```
git clone --branch v2.2 https://gitlab.com/yoloteamdotorg/privssh.git && cd privssh && npm i && npm i -g forever
forever start server.js
```


===========================================

Sau đó vào http://(ip-vps):8080 trong đó (ip-vps) là địa chỉ ip của VPS
