'use strict'
var JwtStrategy = require('passport-jwt').Strategy
var ExtractJwt = require('passport-jwt').ExtractJwt
var User = require('../app/models/user') // load up the user model
var jwtconfig = require('../config/database') // get db config file

module.exports = function (passport) {
  var opts = {}

  opts.secretOrKey = jwtconfig.secret
  opts.issuer = jwtconfig.issuer
  opts.audience = jwtconfig.audience
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader()

  passport.use(
      new JwtStrategy(opts, function (jwtPayload, done) {
        User.findOne({
          userName: jwtPayload.sub
        }, function (err, user) {
          if (err) {
            return done(err, false)
          }

          if (user) {
            done(null, user)
          } else done(null, false, 'User found in token not found')
        })
      })
  )
}
