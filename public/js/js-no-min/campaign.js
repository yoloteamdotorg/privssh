 $(document).ready(function () {
   $('#myModalControl_AutoImport').change(function () {
     $('#myModalControl_AutoImport').bootstrapToggle('disable')
     $('#myModalControl_cMsg').text('')
     socket.emit('control', {
       campaignName: $('#myModalControl_Campaign').text(),
       command: $('#myModalControl_AutoImport').is(':checked') ? 'on' : 'off'
     })
   })

   socket.on('message', function (message) {
     setTimeout(function () {
       $('#myModalControl_AutoImport').bootstrapToggle('enable')
       $('#myModalControl_GetNewSocks').prop('disabled', false)
       $('#myModalControl_cMsg').text(message)
     }, 2000)
   })

   $('#myModalControl_GetNewSocks').click(function () {
     $('#myModalControl_GetNewSocks').prop('disabled', true)
     $('#myModalControl_cMsg').text('')
     socket.emit('control', {
       campaignName: $('#myModalControl_Campaign').text(),
       command: 'getSocks'
     })
   })
 })

 $(document).on('click', '#toolbar_addUser', function () {
   refreshCountryList()

   $('#addSocksNumber').prop('disabled', true)
   $('#unlimited').prop('checked', true)
   $('#myModalAdd_MsgErr').text('')
   $('#myModalAdd_MsgWarn').text('')
   $('#addCampaginName').val('')
   $('#addCountry').tagsinput('removeAll')
   $('#addSocksNumber').attr('placeholder', 'Socks Number')
   $('#myModalAdd').modal('show')
 })

 var refreshCountryList = function () {
   $('#countryList').empty()

   $.ajax({
     type: 'GET',
     url: '/api/admin-socks/Country',
     timeout: 30000,
     beforeSend: function () {

     },
     success: function (result) {
       if ($.isEmptyObject(result)) {
         $('#myModalAdd_MsgErr').text('No Country in Database')
         return
       } else {
         $.each(sortObject(result), function (key, data) {
           $('#countryList').append('<a class="li list-group-item col-md-4" href="#">' + key + '<span class="badge badge-default badge-pill">' + data + '</span></a>')
         })
       }
     },
     complete: function () {
       $('#countryList a.li').click(function (e) {
         e.preventDefault()

         $that = $(this)

         $('#countryList').find('a.li').removeClass('active')
         $('#addSocksNumber').attr('placeholder', $that[0].childNodes[1].innerText)
         $that.addClass('active')
       })
     }
   })

   var sortObject = function (o) {
     var sorted = {}
     var key
     var a = []

     for (key in o) {
       if (o.hasOwnProperty(key)) {
         a.push(key)
       }
     }

     a.sort()

     for (key = 0; key < a.length; key++) {
       sorted[a[key]] = o[a[key]]
     }

     console.log(sorted)

     return sorted
   }
 }

 $('#unlimited').change(function () {
   if ($('#unlimited').is(':checked')) {
     $('#addSocksNumber').prop('disabled', true)
   } else {
     $('#autoRenew').bootstrapToggle('off')
     $('#addSocksNumber').prop('disabled', false)
   }
 })

 $('#_addsocksNumber').click(function (e) {
   if (!$('#countryList').find('.active')[0]) {
     return
   }
   var socksNumber = (parseInt($('#addSocksNumber').val()) > 0) ? $('#addSocksNumber').val() : $('#addSocksNumber').attr('placeholder')
   if (socksNumber > parseInt($('#addSocksNumber').attr('placeholder'))) {
     socksNumber = parseInt($('#addSocksNumber').attr('placeholder'))
     $('#addSocksNumber').val('')
   }
   var country = $('#countryList').find('.active')[0].childNodes[0].data
   $('#addCountry').tagsinput('add', {
     value: country,
     tagText: country + ' : ' + socksNumber,
     number: socksNumber
   })
 })

 var elt = $('#addCountry')
 elt.tagsinput({
   itemValue: 'value',
   itemText: 'tagText',
   allowDuplicates: true
 })

 $(document).on('click', '#edit-row', function () {
   $('#myModalEdit_Msg').text('')
   $('#editPassword').val('')
   $(this).each(function (value, index) {
     var array = $(this).parent().siblings().map(function () {
       return $(this).text().trim()
     }).get()
     $('#editUsername').val(array[1])
     $('#currentPassword').val(document.getElementsByClassName('currentPassword')[array[0] - 1].value)
     $('#editDaysLimit').val(array[3])
     $('#editSocksLimit').val(array[4])
   })
   $('#myModalEdit').modal('show')
 })

 $(document).on('click', '#control-row', function () {
   $('#myModalControl_Msg').text('')
   $(this).each(function (value, index) {
     var array = $(this).parent().siblings().map(function () {
       return $(this).text().trim()
     }).get()

     $('#myModalControl_Campaign').text(array[1])

     if (array[3] === 'Unlimited') {
       $('#autoRenewControl').bootstrapToggle('enable')
       $('#SaveChanges').prop('disabled', false)
     } else {
       $('#autoRenewControl').bootstrapToggle('disable')
       $('#SaveChanges').prop('disabled', true)
     }

     if (array[4] === 'true') {
       $('#autoRenewControl').bootstrapToggle('on')
     } else {
       $('#autoRenewControl').bootstrapToggle('off')
     }
   })

   $('#myModalControl').modal('show')
 })

 $(document).on('click', '#del-row', function () {
   $('#myModalDelete_Msg').text('')
   $(this).each(function () {
     var array = $(this).parent().siblings().map(function () {
       return $(this).text().trim()
     }).get()
     $('#myModalDelete_Campaign').text(array[1])
   })
   $('#myModalDelete').modal('show')
 })

 $(document).ready(function () {
   $('#addCountry').on('beforeItemAdd', function (event) {
     $('#addCountry').tagsinput('items').forEach(function (item) {
       if (event.item.value === item.value) {
         event.cancel = true
         item.number = event.item.number
         item.tagText = event.item.tagText
         $('#addCountry').tagsinput('refresh')
       }
     })
   })

   $('#_refreshCountryList').click(function (e) {
     refreshCountryList()
   })
 })

 $(document).ready(function () {
   $('#addCampagin').click(function (e) {
     $('#myModalAdd_MsgWarn').text('')

     var countryTags = $('#addCountry').tagsinput('items')
     if (!$('#addCampaginName').val()) {
       $('#myModalAdd_MsgWarn').text('Enter Campaign Name')
       return
     }

     if ($.isEmptyObject(countryTags)) {
       $('#myModalAdd_MsgWarn').text('Select at least 1 Country')
       return
     }

     function sortObject (o) {
       o.sort(function (a, b) {
         var nameA = a.value.toUpperCase() // ignore upper and lowercase
         var nameB = b.value.toUpperCase() // ignore upper and lowercase
         if (nameA < nameB) {
           return -1
         }
         if (nameA > nameB) {
           return 1
         }

         // names must be equal
         return 0
       })

       return o
     }

     e.preventDefault()

     console.log($('#autoRenew').val())

     $.ajax({
       type: 'POST',
       url: '/api/admin-campaign/addCampaign',
       timeout: 30000,
       dataType: 'json',
       contentType: 'application/json; charset=UTF-8', // This is the money shot
       data: JSON.stringify({
         campaignName: $('#addCampaginName').val(),
         countryTags: sortObject(countryTags),
         unlimited: $('#unlimited').is(':checked'),
         autoRenew: $('#autoRenew').is(':checked')
       }),
       success: function (results) {
         if (results.success === true) {
           $('#myModalAdd').modal('hide')
           location.reload()
         } else {
           $('#myModalAdd_MsgErr').text(results.msg)
           console.log(results.msg)
         }
       }
     })
   })
 })

 $(document).ready(function () {
   $('#SaveChanges').click(function (e) {
     if ($('#autoRenewControl').prop('disabled')) {
       $('#myModalControl_Msg').text('Save Changes only work with Unlimited Campaign!')
       return
     }
     e.preventDefault()
     $.ajax({
       type: 'POST',
       url: '/api/admin-campaign/editCampaign',
       timeout: 30000,
       data: {
         campaignName: $('#myModalControl_Campaign').text(),
         autoRenew: $('#autoRenewControl').is(':checked')
       },
       success: function (result) {
         if (result.success === true) {
           $('#myModalEdit').modal('hide')
           location.reload()
         } else {
           $('#myModalEdit_Msg').text(result.msg)
           console.log(result.msg)
         }
       }
     })
   })
 })

 $(document).ready(function () {
   $('#yesDelete').click(function (e) {
     if (!$('#myModalDelete_Campaign').text()) {
       return
     }
     e.preventDefault()
     $.ajax({
       type: 'POST',
       url: '/api/admin-campaign/deleteCampaign',
       timeout: 30000,
       data: {
         campaignName: $('#myModalDelete_Campaign').text()
       },
       success: function (result) {
         if (result.success === true) {
           $('#myModalDelete').modal('hide')
           location.reload()
         } else {
           $('#myModalDelete_Msg').text(result.msg)
           console.log(result.msg)
         }
       }
     })
   })
 })
