$(document).on('click', '#toolbar_addUser', function() {
    $('#myModalAdd_Msg').text('');
    $('#addUsername').val('');
    $('#addPassword').val('');
    $('#myModalAdd').modal('show');
});

$(document).on('click', '#edit-row', function() {
    $('#myModalEdit_Msg').text('');
    $('#editPassword').val('');
    $(this).each(function(value, index) {
        var array = $(this).parent().siblings().map(function() {
            return $(this).text().trim();
        }).get();
        $('#editUsername').val(array[1]);
        $('#currentPassword').val(document.getElementsByClassName("currentPassword")[array[0] - 1].value);
        $('#editDaysLimit').val(array[3]);
        $('#editSocksLimit').val(array[4]);
    })
    $('#myModalEdit').modal('show');
});

$(document).on('click', '#del-row', function() {
    $('#myModalDelete_Msg').text('');
    $(this).each(function() {
        var array = $(this).parent().siblings().map(function() {
            return $(this).text().trim();
        }).get();
        $('#myModalDelete_User').text(array[1]);
    })
    $('#myModalDelete').modal('show');
});

$(document).ready(function() {
    $("#addUser").click(function(e) {
        if (!$("#addUsername").val() || !$("#addPassword").val()) { return }
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/api/admin-user/addUser",
            timeout: 30000,
            data: { username: $("#addUsername").val(), password: $("#addPassword").val(), dayslimit: $("#addDaysLimit").val(), sockslimit: $("#addSocksLimit").val() },
            success: function(result) {
                if (result.success === true) {
                    $('#myModalAdd').modal('hide');
                    location.reload();
                } else {
                    $('#myModalAdd_Msg').text(result.msg);
                    console.log(result.msg);
                }
            }
        });
    });
});

$(document).ready(function() {
    $("#SaveChanges").click(function(e) {
        if (!$("#editSocksLimit").val()) { return }
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/api/admin-user/editUser",
            timeout: 30000,
            data: { username: $("#editUsername").val(), crpassword: $("#currentPassword").val(), password: $("#editPassword").val(), dayslimit: $("#editDaysLimit").val(), sockslimit: $("#editSocksLimit").val() },
            success: function(result) {
                if (result.success === true) {
                    $('#myModalEdit').modal('hide');
                    location.reload();
                } else {
                    $('#myModalEdit_Msg').text(result.msg);
                    console.log(result.msg);
                }
            }
        });
    });
});

$(document).ready(function() {
    $("#yesDelete").click(function(e) {
        if (!$("#myModalDelete_User").text()) { return }
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/api/admin-user/deleteUser",
            timeout: 30000,
            data: { username: $("#myModalDelete_User").text() },
            success: function(result) {
                if (result.success === true) {
                    $('#myModalDelete').modal('hide');
                    location.reload();
                } else {
                    $('#myModalDelete_Msg').text(result.msg);
                    console.log(result.msg);
                }
            }
        });
    });
});