var config = require('../config/database'), // get db config file
    jwt = require('jwt-simple'),
    User = require('../app/models/user'), // get the mongoose model
    express = require('express'),
    router = express.Router();

router.get('/', function(req, res) {
    var token = req.cookies.jwt
    if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
        return res.render('login', { title: 'Login' });
    } else {
        var decoded = jwt.decode(token, config.secret);
        User.findOne({
            name: decoded.name,
            isAdmin: true
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                res.render('login', { title: 'Login' });
            } else {
                console.log(user);
                res.redirect('.');
            }
        });
    }
});

router.post('/', function(req, res) {
    if (!req.body.username || !req.body.password || !req.body.username.match('^[a-zA-Z0-9]{4,10}$')) {
        return res.render('login', { title: 'Login', message: "Please enter valid username & password." });
    } else {
        User.findOne({
            username: req.body.username.toLowerCase(),
            isAdmin: true
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                res.render('login', { title: 'Login', message: "Invalid username or password." });
            } else {
                // check if password matches
                user.comparePassword(req.body.password, function(err, isMatch) {
                    if (isMatch && !err) {
                        // if user is found and password is right create a token
                        var token = jwt.encode(user, config.secret);

                        res.cookie('jwt', token, {
                            expires: new Date(Date.now() + 86400000),
                            httpOnly: false
                        });

                        res.redirect('./');
                    } else {
                        res.render('login', { title: 'Login', message: "Invalid username or password." });
                    }
                });
            }
        });
    }
});

module.exports = router;