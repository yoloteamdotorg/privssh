var express = require('express'),
    router = express.Router(),
    config = require('../config/database'),
    User = require('../app/models/user'), // get the mongoose model
    jwt = require('jwt-simple'),
    moment = require("moment");

router.get('/', function(req, res) {
    var token = req.cookies.jwt

    if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
        return res.redirect('/login');
    }

    var decoded = jwt.decode(token, config.secret);
    User.findOne({
        name: decoded.name,
        isAdmin: true
    }, function(err, user) {
        if (err) throw err;

        if (!user) {
            res.redirect('/login');
        } else {
            res.render('socks', { title: 'Socks Manager', version: config.version, time: moment().format("YYYY-MM-DD ~ h:mm:ss A") });
        }
    });
});

module.exports = router;