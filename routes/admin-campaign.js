var config = require('../config/database') // get db config file
var jwt = require('jwt-simple')
var moment = require('moment')
var User = require('../app/models/user') // get the mongoose model
var Campaign = require('../app/models/campaign') // get the mongoose model
var express = require('express')
var router = express.Router()

// route to a restricted info (GET http://localhost:8080/api/memberinfo)
router.get('/', function (req, res, next) {
  var token = req.cookies.jwt

  if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
    return res.redirect('./login')
  }

  var decoded = jwt.decode(token, config.secret)
  User.findOne({
    name: decoded.name,
    isAdmin: true
  }, function (err, user) {
    if (err) throw err

    if (!user) {
      res.redirect('./login')
    } else {
      Campaign.find({}, '-socks -_id -usedSocks', function (err, results) {
        if (err) {
          return res.render('campaign', {
            title: 'Campaign Manager',
            version: config.version,
            time: moment().format('YYYY-MM-DD ~ h:mm:ss A')
          })
        } else {
          console.log(results)
          return res.render('campaign', {
            title: 'Campaign Manager',
            version: config.version,
            time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
            moment: moment,
            rows: results,
            server: process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
            port: (process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080) + 1
          })
        }
      })
    }
  })
})

module.exports = router
