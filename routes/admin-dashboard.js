var config = require('../config/database'), // get db config file
    jwt = require('jwt-simple'),
    modelsSocks = require('../app/models/socks'),
    User = require('../app/models/user'), // get the mongoose model
    dbstats = require('../app/models/dbstats'),
    express = require('express'),
    router = express.Router(),
    moment = require("moment"),
    mongoose = require('mongoose'),
    Q = require('q');

// route to a restricted info (GET http://localhost:8080/api/memberinfo)
router.get('/', function(req, res, next) {
    var token = req.cookies.jwt

    if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
        return res.redirect('./login');
    }

    var decoded = jwt.decode(token, config.secret);
    User.findOne({
        name: decoded.name,
        isAdmin: true
    }, function(err, user) {
        if (err) throw err;

        if (!user) {
            res.redirect('./login');
        } else {
            returnRender(res);
        }
    });
});

var returnRender = function(res) {
    mongoose.connections[0].db.collections(function(error, collections) {
        if (error) {
            throw new Error(error);
            res.render('index', { title: 'Dashboard', version: config.version, totalUser: 0, totalSocks: 0, uploaded: 0, used: 0, time: moment().format("YYYY-MM-DD ~ h:mm:ss A") });
        } else {
            var totalSocks = 0;
            Q.fcall(function() {
                collections.map(function(collection) {
                    if (collection.s.name.match("-Socks")) {
                        collection.count(function(err, countSocks) {
                            if (err) throw err;
                            //console.log(countSocks)
                            totalSocks += countSocks;
                        });
                    }
                });
            }).then(function() {
                setTimeout(function() {
                    User.count({ isAdmin: false }, function(err, countUser) {
                        if (err) throw err;

                        dbstats.findOne({ rootStats: true }, function(err, rootstats) {
                            if (err) throw err;

                            var uploaded, used;
                            if (!rootstats) {
                                uploaded = 0
                                used = 0
                            } else {
                                uploaded = rootstats.uploaded || 0
                                used = rootstats.used || 0
                            }

                            res.render('index', { title: 'Dashboard', version: config.version, totalUser: countUser, totalSocks: totalSocks, uploaded: uploaded, used: used, time: moment().format("YYYY-MM-DD ~ h:mm:ss A") });
                        });
                    });
                }, 100);
            });
        }
    });
};

module.exports = router;