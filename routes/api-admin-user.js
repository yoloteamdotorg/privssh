var config = require('../config/database'), // get db config file
    User = require('../app/models/user'), // get the mongoose model
    bcrypt = require('bcryptjs'),
    express = require('express'),
    router = express.Router(),
    moment = require("moment");

router.post('/addUser', function(req, res) {
    if (!req.body.username || !req.body.password || !req.body.username.match('^[a-zA-Z0-9]+$')) {
        return res.json({
            success: false,
            time: moment().format("YYYY-MM-DD ~ h:mm:ss A"),
            msg: 'Please enter valid username & password.'
        });
    } else {
        var newUser = new User({
            username: req.body.username.toLowerCase(),
            password: req.body.password,
            isAdmin: false,
            daysLimit: req.body.dayslimit || 30,
            socksLimit: req.body.sockslimit || 100,
            createdOn: new Date()
        });
        // save the user
        newUser.save(function(err) {
            if (err && (11000 === err.code || 11001 === err.code)) {
                return res.json({
                    success: false,
                    time: moment().format("YYYY-MM-DD ~ h:mm:ss A"),
                    msg: 'Username already exists.'
                });
            }

            return res.json({
                success: true,
                time: moment().format("YYYY-MM-DD ~ h:mm:ss A"),
                msg: 'Successful created new user.'
            });
        });
    }
});

router.post('/editUser', function(req, res) {
    if (!req.body.username || !req.body.username.match('^[a-zA-Z0-9]+$')) {
        return res.json({
            success: false,
            time: moment().format("YYYY-MM-DD ~ h:mm:ss A"),
            msg: 'Please enter valid username & password.'
        });
    } else {
        User.encryptPassword(req.body.password, function(err, hash) {
            if (err) {
                throw err;
            }

            User.findOneAndUpdate({
                username: req.body.username,
                isAdmin: false
            }, {
                username: req.body.username,
                password: hash || req.body.crpassword,
                daysLimit: req.body.dayslimit || 30,
                socksLimit: req.body.sockslimit || 100
            }, { new: true }, function(err, results) {
                if (err) {
                    console.log(err);
                    return res.json({
                        success: false,
                        time: moment().format("YYYY-MM-DD ~ h:mm:ss A"),
                        msg: "Can't update user."
                    });
                } else {
                    console.log(results);
                    return res.json({
                        success: true,
                        time: moment().format("YYYY-MM-DD ~ h:mm:ss A"),
                        msg: 'Successful update user.'
                    });
                }
            });
        });
    }
});

router.post('/deleteUser', function(req, res) {
    if (!req.body.username) {
        res.json({
            success: false,
            time: moment().format("YYYY-MM-DD ~ h:mm:ss A"),
            msg: 'Please enter valid username & password.'
        });
    } else {
        User.findOneAndRemove({
            username: req.body.username,
            isAdmin: false
        }, function(err, results) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    time: moment().format("YYYY-MM-DD ~ h:mm:ss A"),
                    msg: "Can't detele user"
                });
            } else {
                console.log(results);
                return res.json({
                    success: true,
                    time: moment().format("YYYY-MM-DD ~ h:mm:ss A"),
                    msg: 'Successful detele user.'
                });
            }
        });
    }
});

module.exports = router;