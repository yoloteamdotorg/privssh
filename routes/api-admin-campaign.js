var Socks = require('../app/models/socks')
var Campaign = require('../app/models/campaign')

var express = require('express')
var router = express.Router()
var moment = require('moment')
var Q = require('q')

router.post('/addCampaign', function (req, res) {
  console.log(req.body)

  if (!req.body.campaignName || !req.body.countryTags || req.body.unlimited === null) {
    return res.json({
      success: false,
      time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
      msg: 'Please enter valid data'
    })
  } else {
    Campaign.findOne({
      campaignName: req.body.campaignName.toLowerCase()
    }, function (err, campaign) {
      if (err) throw err
      if (campaign) {
        return res.json({
          success: false,
          time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
          msg: 'Campaign already exists.'
        })
      } else {
        initCampaign(req).then(function (initCampaign) {
          var newCampaign = new Campaign({
            campaignName: req.body.campaignName,
            unlimited: req.body.unlimited,
            autoRenew: req.body.autoRenew,
            country: initCampaign[0],
            socks: initCampaign[1],
            usedSocks: initCampaign[2],
            totalSocks: initCampaign[3]
          })

          newCampaign.save(function (err, cb) {
            if (err && (err.code === 11000 || err.code === 11001)) {
              return res.json({
                success: false,
                time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
                msg: "Can't save. Campaign already exists."
              })
            }

            return res.json({
              success: true,
              time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
              msg: 'Successful created new Campaign.'
            })
          })
        })
      }
    })
  }
})

router.post('/editCampaign', function (req, res) {
  if (!req.body.campaignName || !req.body.autoRenew) {
    return res.json({
      success: false,
      time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
      msg: 'Please enter valid data'
    })
  }

  Campaign.findOneAndUpdate({
    campaignName: req.body.campaignName.toLowerCase()
  }, {
    autoRenew: req.body.autoRenew
  }, function (err, result) {
    if (err) throw err

    console.log(result)

    return res.json({
      success: true,
      time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
      msg: 'Successful edit campaign.'
    })
  })
})

router.post('/deleteCampaign', function (req, res) {
  if (!req.body.campaignName) {
    res.json({
      success: false,
      time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
      msg: 'No campaign name'
    })
  } else {
    Campaign.findOneAndRemove({
      campaignName: req.body.campaignName
    }, function (err, results) {
      if (err) {
        console.log(err)
        return res.json({
          success: false,
          time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
          msg: "Can't detele campaign"
        })
      } else {
        console.log(results)
        return res.json({
          success: true,
          time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
          msg: 'Successful detele campaign.'
        })
      }
    })
  }
})

var initCampaign = function (req) {
  var deferred3 = Q.defer()

  Q.fcall(function () {
    var _totalSocks = 0
    var _country = []
    var _socks = []

    var deferred2 = Q.defer()

    req.body.countryTags.forEach(function (element) {
      var countryModel = getModelSocksByCountry(element.value)

      if (req.body.unlimited === false) {
        countryModel.getRandomSocksCountryById(element.number, function (err, rdnSocks) {
          if (err) throw err
          var oSocks = {}
          Q.fcall(function () {
            var deferred1 = Q.defer()
            var _listSocks = []

            for (var socks in rdnSocks) {
              _listSocks.push(rdnSocks[socks]._id)
            }

            setTimeout(function () {
              deferred1.resolve(_listSocks)
            }, 100)

            return deferred1.promise
          }).then(function (_listSocks) {
            _country.push(element.value)
            oSocks['socks'] = _listSocks
            oSocks['country'] = element.value
            _socks.push(oSocks)
            _totalSocks += _listSocks.length
            setTimeout(function () {
              deferred2.resolve([_country, _socks, [], _totalSocks])
            }, 150)
          })
        })
      } else {
        _country.push(element.value)
        setTimeout(function () {
          deferred2.resolve([_country, [],
            [], -1
          ])
        }, 150)
      }
    })

    return deferred2.promise
  }).then(function (countryTags) {
    setTimeout(function () {
      deferred3.resolve(countryTags)
    }, 100)
  })

  return deferred3.promise
}

function getModelSocksByCountry (country) {
  var modelsSocks
  switch (country.toUpperCase()) {
    case 'AF':
      modelsSocks = Socks.AF
      break
    case 'AL':
      modelsSocks = Socks.AL
      break
    case 'DZ':
      modelsSocks = Socks.DZ
      break
    case 'AS':
      modelsSocks = Socks.AS
      break
    case 'AD':
      modelsSocks = Socks.AD
      break
    case 'AO':
      modelsSocks = Socks.AO
      break
    case 'AI':
      modelsSocks = Socks.AI
      break
    case 'AQ':
      modelsSocks = Socks.AQ
      break
    case 'AG':
      modelsSocks = Socks.AG
      break
    case 'AR':
      modelsSocks = Socks.AR
      break
    case 'AM':
      modelsSocks = Socks.AM
      break
    case 'AW':
      modelsSocks = Socks.AW
      break
    case 'AU':
      modelsSocks = Socks.AU
      break
    case 'AT':
      modelsSocks = Socks.AT
      break
    case 'AZ':
      modelsSocks = Socks.AZ
      break
    case 'BS':
      modelsSocks = Socks.BS
      break
    case 'BH':
      modelsSocks = Socks.BH
      break
    case 'BD':
      modelsSocks = Socks.BD
      break
    case 'BB':
      modelsSocks = Socks.BB
      break
    case 'BY':
      modelsSocks = Socks.BY
      break
    case 'BE':
      modelsSocks = Socks.BE
      break
    case 'BZ':
      modelsSocks = Socks.BZ
      break
    case 'BJ':
      modelsSocks = Socks.BJ
      break
    case 'BM':
      modelsSocks = Socks.BM
      break
    case 'BT':
      modelsSocks = Socks.BT
      break
    case 'BO':
      modelsSocks = Socks.BO
      break
    case 'BA':
      modelsSocks = Socks.BA
      break
    case 'BW':
      modelsSocks = Socks.BW
      break
    case 'BR':
      modelsSocks = Socks.BR
      break
    case 'IO':
      modelsSocks = Socks.IO
      break
    case 'VG':
      modelsSocks = Socks.VG
      break
    case 'BN':
      modelsSocks = Socks.BN
      break
    case 'BG':
      modelsSocks = Socks.BG
      break
    case 'BF':
      modelsSocks = Socks.BF
      break
    case 'BI':
      modelsSocks = Socks.BI
      break
    case 'KH':
      modelsSocks = Socks.KH
      break
    case 'CM':
      modelsSocks = Socks.CM
      break
    case 'CA':
      modelsSocks = Socks.CA
      break
    case 'CV':
      modelsSocks = Socks.CV
      break
    case 'KY':
      modelsSocks = Socks.KY
      break
    case 'CF':
      modelsSocks = Socks.CF
      break
    case 'TD':
      modelsSocks = Socks.TD
      break
    case 'CL':
      modelsSocks = Socks.CL
      break
    case 'CN':
      modelsSocks = Socks.CN
      break
    case 'CX':
      modelsSocks = Socks.CX
      break
    case 'CC':
      modelsSocks = Socks.CC
      break
    case 'CO':
      modelsSocks = Socks.CO
      break
    case 'KM':
      modelsSocks = Socks.KM
      break
    case 'CK':
      modelsSocks = Socks.CK
      break
    case 'CR':
      modelsSocks = Socks.CR
      break
    case 'HR':
      modelsSocks = Socks.HR
      break
    case 'CU':
      modelsSocks = Socks.CU
      break
    case 'CW':
      modelsSocks = Socks.CW
      break
    case 'CY':
      modelsSocks = Socks.CY
      break
    case 'CZ':
      modelsSocks = Socks.CZ
      break
    case 'CD':
      modelsSocks = Socks.CD
      break
    case 'DK':
      modelsSocks = Socks.DK
      break
    case 'DJ':
      modelsSocks = Socks.DJ
      break
    case 'DM':
      modelsSocks = Socks.DM
      break
    case 'DO':
      modelsSocks = Socks.DO
      break
    case 'TL':
      modelsSocks = Socks.TL
      break
    case 'EC':
      modelsSocks = Socks.EC
      break
    case 'EG':
      modelsSocks = Socks.EG
      break
    case 'SV':
      modelsSocks = Socks.SV
      break
    case 'GQ':
      modelsSocks = Socks.GQ
      break
    case 'ER':
      modelsSocks = Socks.ER
      break
    case 'EE':
      modelsSocks = Socks.EE
      break
    case 'ET':
      modelsSocks = Socks.ET
      break
    case 'FK':
      modelsSocks = Socks.FK
      break
    case 'FO':
      modelsSocks = Socks.FO
      break
    case 'FJ':
      modelsSocks = Socks.FJ
      break
    case 'FI':
      modelsSocks = Socks.FI
      break
    case 'FR':
      modelsSocks = Socks.FR
      break
    case 'PF':
      modelsSocks = Socks.PF
      break
    case 'GA':
      modelsSocks = Socks.GA
      break
    case 'GM':
      modelsSocks = Socks.GM
      break
    case 'GE':
      modelsSocks = Socks.GE
      break
    case 'DE':
      modelsSocks = Socks.DE
      break
    case 'GH':
      modelsSocks = Socks.GH
      break
    case 'GI':
      modelsSocks = Socks.GI
      break
    case 'GR':
      modelsSocks = Socks.GR
      break
    case 'GL':
      modelsSocks = Socks.GL
      break
    case 'GD':
      modelsSocks = Socks.GD
      break
    case 'GU':
      modelsSocks = Socks.GU
      break
    case 'GT':
      modelsSocks = Socks.GT
      break
    case 'GG':
      modelsSocks = Socks.GG
      break
    case 'GN':
      modelsSocks = Socks.GN
      break
    case 'GW':
      modelsSocks = Socks.GW
      break
    case 'GY':
      modelsSocks = Socks.GY
      break
    case 'HT':
      modelsSocks = Socks.HT
      break
    case 'HN':
      modelsSocks = Socks.HN
      break
    case 'HK':
      modelsSocks = Socks.HK
      break
    case 'HU':
      modelsSocks = Socks.HU
      break
    case 'IS':
      modelsSocks = Socks.IS
      break
    case 'IN':
      modelsSocks = Socks.IN
      break
    case 'ID':
      modelsSocks = Socks.ID
      break
    case 'IR':
      modelsSocks = Socks.IR
      break
    case 'IQ':
      modelsSocks = Socks.IQ
      break
    case 'IE':
      modelsSocks = Socks.IE
      break
    case 'IM':
      modelsSocks = Socks.IM
      break
    case 'IL':
      modelsSocks = Socks.IL
      break
    case 'IT':
      modelsSocks = Socks.IT
      break
    case 'CI':
      modelsSocks = Socks.CI
      break
    case 'JM':
      modelsSocks = Socks.JM
      break
    case 'JP':
      modelsSocks = Socks.JP
      break
    case 'JE':
      modelsSocks = Socks.JE
      break
    case 'JO':
      modelsSocks = Socks.JO
      break
    case 'KZ':
      modelsSocks = Socks.KZ
      break
    case 'KE':
      modelsSocks = Socks.KE
      break
    case 'KI':
      modelsSocks = Socks.KI
      break
    case 'XK':
      modelsSocks = Socks.XK
      break
    case 'KW':
      modelsSocks = Socks.KW
      break
    case 'KG':
      modelsSocks = Socks.KG
      break
    case 'LA':
      modelsSocks = Socks.LA
      break
    case 'LV':
      modelsSocks = Socks.LV
      break
    case 'LB':
      modelsSocks = Socks.LB
      break
    case 'LS':
      modelsSocks = Socks.LS
      break
    case 'LR':
      modelsSocks = Socks.LR
      break
    case 'LY':
      modelsSocks = Socks.LY
      break
    case 'LI':
      modelsSocks = Socks.LI
      break
    case 'LT':
      modelsSocks = Socks.LT
      break
    case 'LU':
      modelsSocks = Socks.LU
      break
    case 'MO':
      modelsSocks = Socks.MO
      break
    case 'MK':
      modelsSocks = Socks.MK
      break
    case 'MG':
      modelsSocks = Socks.MG
      break
    case 'MW':
      modelsSocks = Socks.MW
      break
    case 'MY':
      modelsSocks = Socks.MY
      break
    case 'MV':
      modelsSocks = Socks.MV
      break
    case 'ML':
      modelsSocks = Socks.ML
      break
    case 'MT':
      modelsSocks = Socks.MT
      break
    case 'MH':
      modelsSocks = Socks.MH
      break
    case 'MR':
      modelsSocks = Socks.MR
      break
    case 'MU':
      modelsSocks = Socks.MU
      break
    case 'YT':
      modelsSocks = Socks.YT
      break
    case 'MX':
      modelsSocks = Socks.MX
      break
    case 'FM':
      modelsSocks = Socks.FM
      break
    case 'MD':
      modelsSocks = Socks.MD
      break
    case 'MC':
      modelsSocks = Socks.MC
      break
    case 'MN':
      modelsSocks = Socks.MN
      break
    case 'ME':
      modelsSocks = Socks.ME
      break
    case 'MS':
      modelsSocks = Socks.MS
      break
    case 'MA':
      modelsSocks = Socks.MA
      break
    case 'MZ':
      modelsSocks = Socks.MZ
      break
    case 'MM':
      modelsSocks = Socks.MM
      break
    case 'NA':
      modelsSocks = Socks.NA
      break
    case 'NR':
      modelsSocks = Socks.NR
      break
    case 'NP':
      modelsSocks = Socks.NP
      break
    case 'NL':
      modelsSocks = Socks.NL
      break
    case 'AN':
      modelsSocks = Socks.AN
      break
    case 'NC':
      modelsSocks = Socks.NC
      break
    case 'NZ':
      modelsSocks = Socks.NZ
      break
    case 'NI':
      modelsSocks = Socks.NI
      break
    case 'NE':
      modelsSocks = Socks.NE
      break
    case 'NG':
      modelsSocks = Socks.NG
      break
    case 'NU':
      modelsSocks = Socks.NU
      break
    case 'KP':
      modelsSocks = Socks.KP
      break
    case 'MP':
      modelsSocks = Socks.MP
      break
    case 'NO':
      modelsSocks = Socks.NO
      break
    case 'OM':
      modelsSocks = Socks.OM
      break
    case 'PK':
      modelsSocks = Socks.PK
      break
    case 'PW':
      modelsSocks = Socks.PW
      break
    case 'PS':
      modelsSocks = Socks.PS
      break
    case 'PA':
      modelsSocks = Socks.PA
      break
    case 'PG':
      modelsSocks = Socks.PG
      break
    case 'PY':
      modelsSocks = Socks.PY
      break
    case 'PE':
      modelsSocks = Socks.PE
      break
    case 'PH':
      modelsSocks = Socks.PH
      break
    case 'PN':
      modelsSocks = Socks.PN
      break
    case 'PL':
      modelsSocks = Socks.PL
      break
    case 'PT':
      modelsSocks = Socks.PT
      break
    case 'PR':
      modelsSocks = Socks.PR
      break
    case 'QA':
      modelsSocks = Socks.QA
      break
    case 'CG':
      modelsSocks = Socks.CG
      break
    case 'RE':
      modelsSocks = Socks.RE
      break
    case 'RO':
      modelsSocks = Socks.RO
      break
    case 'RU':
      modelsSocks = Socks.RU
      break
    case 'RW':
      modelsSocks = Socks.RW
      break
    case 'BL':
      modelsSocks = Socks.BL
      break
    case 'SH':
      modelsSocks = Socks.SH
      break
    case 'KN':
      modelsSocks = Socks.KN
      break
    case 'LC':
      modelsSocks = Socks.LC
      break
    case 'MF':
      modelsSocks = Socks.MF
      break
    case 'PM':
      modelsSocks = Socks.PM
      break
    case 'VC':
      modelsSocks = Socks.VC
      break
    case 'WS':
      modelsSocks = Socks.WS
      break
    case 'SM':
      modelsSocks = Socks.SM
      break
    case 'ST':
      modelsSocks = Socks.ST
      break
    case 'SA':
      modelsSocks = Socks.SA
      break
    case 'SN':
      modelsSocks = Socks.SN
      break
    case 'RS':
      modelsSocks = Socks.RS
      break
    case 'SC':
      modelsSocks = Socks.SC
      break
    case 'SL':
      modelsSocks = Socks.SL
      break
    case 'SG':
      modelsSocks = Socks.SG
      break
    case 'SX':
      modelsSocks = Socks.SX
      break
    case 'SK':
      modelsSocks = Socks.SK
      break
    case 'SI':
      modelsSocks = Socks.SI
      break
    case 'SB':
      modelsSocks = Socks.SB
      break
    case 'SO':
      modelsSocks = Socks.SO
      break
    case 'ZA':
      modelsSocks = Socks.ZA
      break
    case 'KR':
      modelsSocks = Socks.KR
      break
    case 'SS':
      modelsSocks = Socks.SS
      break
    case 'ES':
      modelsSocks = Socks.ES
      break
    case 'LK':
      modelsSocks = Socks.LK
      break
    case 'SD':
      modelsSocks = Socks.SD
      break
    case 'SR':
      modelsSocks = Socks.SR
      break
    case 'SJ':
      modelsSocks = Socks.SJ
      break
    case 'SZ':
      modelsSocks = Socks.SZ
      break
    case 'SE':
      modelsSocks = Socks.SE
      break
    case 'CH':
      modelsSocks = Socks.CH
      break
    case 'SY':
      modelsSocks = Socks.SY
      break
    case 'TW':
      modelsSocks = Socks.TW
      break
    case 'TJ':
      modelsSocks = Socks.TJ
      break
    case 'TZ':
      modelsSocks = Socks.TZ
      break
    case 'TH':
      modelsSocks = Socks.TH
      break
    case 'TG':
      modelsSocks = Socks.TG
      break
    case 'TK':
      modelsSocks = Socks.TK
      break
    case 'TO':
      modelsSocks = Socks.TO
      break
    case 'TT':
      modelsSocks = Socks.TT
      break
    case 'TN':
      modelsSocks = Socks.TN
      break
    case 'TR':
      modelsSocks = Socks.TR
      break
    case 'TM':
      modelsSocks = Socks.TM
      break
    case 'TC':
      modelsSocks = Socks.TC
      break
    case 'TV':
      modelsSocks = Socks.TV
      break
    case 'VI':
      modelsSocks = Socks.VI
      break
    case 'UG':
      modelsSocks = Socks.UG
      break
    case 'UA':
      modelsSocks = Socks.UA
      break
    case 'AE':
      modelsSocks = Socks.AE
      break
    case 'GB':
      modelsSocks = Socks.GB
      break
    case 'US':
      modelsSocks = Socks.US
      break
    case 'UY':
      modelsSocks = Socks.UY
      break
    case 'UZ':
      modelsSocks = Socks.UZ
      break
    case 'VU':
      modelsSocks = Socks.VU
      break
    case 'VA':
      modelsSocks = Socks.VA
      break
    case 'VE':
      modelsSocks = Socks.VE
      break
    case 'VN':
      modelsSocks = Socks.VN
      break
    case 'WF':
      modelsSocks = Socks.WF
      break
    case 'EH':
      modelsSocks = Socks.EH
      break
    case 'YE':
      modelsSocks = Socks.YE
      break
    case 'ZM':
      modelsSocks = Socks.ZM
      break
    case 'ZW':
      modelsSocks = Socks.ZW
      break
    default:
      modelsSocks = Socks.UKNW
  }
  return modelsSocks
}

module.exports = router
