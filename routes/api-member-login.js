var config = require('../config/database'), // get db config file
    jwt = require('jwt-simple'),
    User = require('../app/models/user'), // get the mongoose model
    express = require('express'),
    moment = require("moment"),
    router = express.Router();

router.post('/', function(req, res) {
    if (!req.body.username || !req.body.password) {
        res.json({
            success: false,
            msg: 'Please enter name and password. :-?'
        });
    } else {
        User.findOne({
            username: req.body.username,
            isAdmin: false
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                res.json({
                    success: false,
                    msg: 'User not found. >:]'
                });
            } else {
                // check if password matches
                user.comparePassword(req.body.password, function(err, isMatch) {
                    if (isMatch && !err) {
                        var timeDiff = moment().diff(user.createdOn, 'hours')
                        if (timeDiff < user.daysLimit * 24) {
                            var token = jwt.encode(user, config.secret);
                            res.json({
                                success: true,
                                LimitSocks: user.socksLimit,
                                expired: user.daysLimit * 24 - timeDiff,
                                token: token
                            });
                        } else {
                            res.json({
                                success: false,
                                msg: 'Your account was expired. :D'
                            });
                        }
                    } else {
                        res.json({
                            success: false,
                            msg: 'Wrong password! :/'
                        });
                    }
                });
            }
        });
    }
});

module.exports = router;