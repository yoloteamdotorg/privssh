var User = require('../app/models/user')
var Socks = require('../app/models/socks')

var express = require('express')
var router = express.Router()
var config = require('../config/database')
var jwt = require('jwt-simple')
var mongoose = require('mongoose')
var Q = require('q')
var moment = require('moment')

router.post('/SocksResetAll', function (req, res) {
  var token = req.cookies.jwt

  if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
    return res.redirect('/login')
  }

  var decoded = jwt.decode(token, config.secret)
  User.findOne({
    name: decoded.name,
    isAdmin: true
  }, function (err, user) {
    if (err) throw err

    if (!user) {
      res.redirect('/login')
    } else {
      var removed = 0
      mongoose.connections[0].db.collections(function (error, collections) {
        if (error) throw err

        Q.fcall(function () {
          collections.map(function (collection) {
            if (collection.s.name.match('-Socks')) {
              mongoose.connection.db.collection(collection.s.name).remove(function (err, results) {
                if (err) throw err
                removed += results.result.n
                  // console.log("remove: " + removed);
              })
            }
          })
        }).then(function () {
          setTimeout(function () {
            console.log('All items completed! ' + removed)

            res.json({
              success: true,
              time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
              result: removed || 0
            })
          }, 100)
        })
      })
    }
  })
})

router.post('/SocksResetByCountry', function (req, res) {
  var token = req.cookies.jwt

  console.log(req.body.reset_country)

  if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
    return res.redirect('/login')
  }

  if (!req.body.reset_country || !req.body.reset_country.length || !req.body.reset_country.trim()) {
    return res.redirect('/socks')
  }

  var decoded = jwt.decode(token, config.secret)
  User.findOne({
    name: decoded.name,
    isAdmin: true
  }, function (err, user) {
    if (err) throw err

    if (!user) {
      res.redirect('/login')
    } else {
      getModelSocksByCountry(req.body.reset_country).collection.remove(function (err, results) {
        if (err) throw err

        res.json({
          success: true,
          time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
          removed: results.result.n || 0
        })
      })
    }
  })
})

router.post('/SocksResetByDate', function (req, res) {
  var token = req.cookies.jwt

  if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
    return res.redirect('/login')
  }

  if (!req.body.datePicker || !req.body.datePicker.length || !req.body.datePicker.trim()) {
    return res.redirect('/socks')
  }

  var decoded = jwt.decode(token, config.secret)
  User.findOne({
    name: decoded.name,
    isAdmin: true
  }, function (err, user) {
    if (err) throw err

    if (!user) {
      res.redirect('/login')
    } else {
      mongoose.connections[0].db.collections(function (error, collections) {
        if (error) throw error

        var removed = 0
        Q.fcall(function () {
          collections.map(function (collection) {
            if (collection.s.name.match('-Socks')) {
              mongoose.connection.db.collection(collection.s.name).remove({
                createtOn: moment(req.body.datePicker).format('YYYY-MM-DD')
              },
                  function (err, results) {
                    if (err) throw err
                    // console.log(removed);
                    removed += results.result.n
                  })
            }
          })
        }).then(function () {
          setTimeout(function () {
            res.json({
              success: true,
              time: moment().format('YYYY-MM-DD ~ h:mm:ss A'),
              result: removed || 0
            })
          }, 100)
        })
      })
    }
  })
})

router.post('/UploadAPIKey', function (req, res) {
  var token = req.cookies.jwt

  if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
    return res.redirect('/login')
  }

  var decoded = jwt.decode(token, config.secret)
  User.findOne({
    name: decoded.name,
    isAdmin: true
  }, function (err, admin) {
    if (err) throw err

    if (!admin) {
      res.redirect('/login')
    } else {
      var rand = function () {
        return Math.random().toString(36).substr(2)
      }

      var _apikey = new Buffer(rand()).toString('base64')

      admin.update({
        upload_apikey: _apikey
      }, function (err) {
        setTimeout(function () {
          if (!err) {
            res.json({
              success: true,
              msg: _apikey
            })
          } else {
            res.json({
              success: true,
              msg: "Can't save autoupload_apikey"
            })
          }
        }, 800)
      })
    }
  })
})

router.post('/GetSocksAPIKey', function (req, res) {
  var token = req.cookies.jwt

  if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
    return res.redirect('/login')
  }

  var decoded = jwt.decode(token, config.secret)
  User.findOne({
    name: decoded.name,
    isAdmin: true
  }, function (err, admin) {
    if (err) throw err

    if (!admin) {
      res.redirect('/login')
    } else {
      var rand = function () {
        return Math.random().toString(36).substr(2)
      }

      var _apikey = new Buffer(rand()).toString('base64')

      admin.update({
        getsocks_apikey: _apikey
      }, function (err) {
        setTimeout(function () {
          if (!err) {
            res.json({
              success: true,
              msg: _apikey
            })
          } else {
            res.json({
              success: true,
              msg: "Can't save autoupload_apikey"
            })
          }
        }, 800)
      })
    }
  })
})

router.get('/Country', function (req, res) {
  var token = req.cookies.jwt

  if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
    return res.redirect('/login')
  }

  var decoded = jwt.decode(token, config.secret)
  User.findOne({
    name: decoded.name,
    isAdmin: true
  }, function (err, admin) {
    if (err) throw err

    if (!admin) {
      res.redirect('/login')
    } else {
      mongoose.connections[0].db.collections(function (error, collections) {
        if (error) throw error
        var totalCountry = {}
        Q.fcall(function () {
          collections.map(function (collection) {
            if (collection.s.name.match('-Socks')) {
              collection.count(function (err, countSocks) {
                if (err) throw err
                if (countSocks > 0) {
                  totalCountry[collection.s.name.substring(0, collection.s.name.indexOf('-'))] = countSocks
                }
              })
            }
          })
        }).then(function () {
          setTimeout(function () {
            res.json(totalCountry)
          }, 100)
        })
      })
    }
  })
})

var getModelSocksByCountry = function (country) {
  var modelSocks
  switch (country.toUpperCase()) {
    case 'AF':
      modelSocks = Socks.AF
      break
    case 'AL':
      modelSocks = Socks.AL
      break
    case 'DZ':
      modelSocks = Socks.DZ
      break
    case 'AS':
      modelSocks = Socks.AS
      break
    case 'AD':
      modelSocks = Socks.AD
      break
    case 'AO':
      modelSocks = Socks.AO
      break
    case 'AI':
      modelSocks = Socks.AI
      break
    case 'AQ':
      modelSocks = Socks.AQ
      break
    case 'AG':
      modelSocks = Socks.AG
      break
    case 'AR':
      modelSocks = Socks.AR
      break
    case 'AM':
      modelSocks = Socks.AM
      break
    case 'AW':
      modelSocks = Socks.AW
      break
    case 'AU':
      modelSocks = Socks.AU
      break
    case 'AT':
      modelSocks = Socks.AT
      break
    case 'AZ':
      modelSocks = Socks.AZ
      break
    case 'BS':
      modelSocks = Socks.BS
      break
    case 'BH':
      modelSocks = Socks.BH
      break
    case 'BD':
      modelSocks = Socks.BD
      break
    case 'BB':
      modelSocks = Socks.BB
      break
    case 'BY':
      modelSocks = Socks.BY
      break
    case 'BE':
      modelSocks = Socks.BE
      break
    case 'BZ':
      modelSocks = Socks.BZ
      break
    case 'BJ':
      modelSocks = Socks.BJ
      break
    case 'BM':
      modelSocks = Socks.BM
      break
    case 'BT':
      modelSocks = Socks.BT
      break
    case 'BO':
      modelSocks = Socks.BO
      break
    case 'BA':
      modelSocks = Socks.BA
      break
    case 'BW':
      modelSocks = Socks.BW
      break
    case 'BR':
      modelSocks = Socks.BR
      break
    case 'IO':
      modelSocks = Socks.IO
      break
    case 'VG':
      modelSocks = Socks.VG
      break
    case 'BN':
      modelSocks = Socks.BN
      break
    case 'BG':
      modelSocks = Socks.BG
      break
    case 'BF':
      modelSocks = Socks.BF
      break
    case 'BI':
      modelSocks = Socks.BI
      break
    case 'KH':
      modelSocks = Socks.KH
      break
    case 'CM':
      modelSocks = Socks.CM
      break
    case 'CA':
      modelSocks = Socks.CA
      break
    case 'CV':
      modelSocks = Socks.CV
      break
    case 'KY':
      modelSocks = Socks.KY
      break
    case 'CF':
      modelSocks = Socks.CF
      break
    case 'TD':
      modelSocks = Socks.TD
      break
    case 'CL':
      modelSocks = Socks.CL
      break
    case 'CN':
      modelSocks = Socks.CN
      break
    case 'CX':
      modelSocks = Socks.CX
      break
    case 'CC':
      modelSocks = Socks.CC
      break
    case 'CO':
      modelSocks = Socks.CO
      break
    case 'KM':
      modelSocks = Socks.KM
      break
    case 'CK':
      modelSocks = Socks.CK
      break
    case 'CR':
      modelSocks = Socks.CR
      break
    case 'HR':
      modelSocks = Socks.HR
      break
    case 'CU':
      modelSocks = Socks.CU
      break
    case 'CW':
      modelSocks = Socks.CW
      break
    case 'CY':
      modelSocks = Socks.CY
      break
    case 'CZ':
      modelSocks = Socks.CZ
      break
    case 'CD':
      modelSocks = Socks.CD
      break
    case 'DK':
      modelSocks = Socks.DK
      break
    case 'DJ':
      modelSocks = Socks.DJ
      break
    case 'DM':
      modelSocks = Socks.DM
      break
    case 'DO':
      modelSocks = Socks.DO
      break
    case 'TL':
      modelSocks = Socks.TL
      break
    case 'EC':
      modelSocks = Socks.EC
      break
    case 'EG':
      modelSocks = Socks.EG
      break
    case 'SV':
      modelSocks = Socks.SV
      break
    case 'GQ':
      modelSocks = Socks.GQ
      break
    case 'ER':
      modelSocks = Socks.ER
      break
    case 'EE':
      modelSocks = Socks.EE
      break
    case 'ET':
      modelSocks = Socks.ET
      break
    case 'FK':
      modelSocks = Socks.FK
      break
    case 'FO':
      modelSocks = Socks.FO
      break
    case 'FJ':
      modelSocks = Socks.FJ
      break
    case 'FI':
      modelSocks = Socks.FI
      break
    case 'FR':
      modelSocks = Socks.FR
      break
    case 'PF':
      modelSocks = Socks.PF
      break
    case 'GA':
      modelSocks = Socks.GA
      break
    case 'GM':
      modelSocks = Socks.GM
      break
    case 'GE':
      modelSocks = Socks.GE
      break
    case 'DE':
      modelSocks = Socks.DE
      break
    case 'GH':
      modelSocks = Socks.GH
      break
    case 'GI':
      modelSocks = Socks.GI
      break
    case 'GR':
      modelSocks = Socks.GR
      break
    case 'GL':
      modelSocks = Socks.GL
      break
    case 'GD':
      modelSocks = Socks.GD
      break
    case 'GU':
      modelSocks = Socks.GU
      break
    case 'GT':
      modelSocks = Socks.GT
      break
    case 'GG':
      modelSocks = Socks.GG
      break
    case 'GN':
      modelSocks = Socks.GN
      break
    case 'GW':
      modelSocks = Socks.GW
      break
    case 'GY':
      modelSocks = Socks.GY
      break
    case 'HT':
      modelSocks = Socks.HT
      break
    case 'HN':
      modelSocks = Socks.HN
      break
    case 'HK':
      modelSocks = Socks.HK
      break
    case 'HU':
      modelSocks = Socks.HU
      break
    case 'IS':
      modelSocks = Socks.IS
      break
    case 'IN':
      modelSocks = Socks.IN
      break
    case 'ID':
      modelSocks = Socks.ID
      break
    case 'IR':
      modelSocks = Socks.IR
      break
    case 'IQ':
      modelSocks = Socks.IQ
      break
    case 'IE':
      modelSocks = Socks.IE
      break
    case 'IM':
      modelSocks = Socks.IM
      break
    case 'IL':
      modelSocks = Socks.IL
      break
    case 'IT':
      modelSocks = Socks.IT
      break
    case 'CI':
      modelSocks = Socks.CI
      break
    case 'JM':
      modelSocks = Socks.JM
      break
    case 'JP':
      modelSocks = Socks.JP
      break
    case 'JE':
      modelSocks = Socks.JE
      break
    case 'JO':
      modelSocks = Socks.JO
      break
    case 'KZ':
      modelSocks = Socks.KZ
      break
    case 'KE':
      modelSocks = Socks.KE
      break
    case 'KI':
      modelSocks = Socks.KI
      break
    case 'XK':
      modelSocks = Socks.XK
      break
    case 'KW':
      modelSocks = Socks.KW
      break
    case 'KG':
      modelSocks = Socks.KG
      break
    case 'LA':
      modelSocks = Socks.LA
      break
    case 'LV':
      modelSocks = Socks.LV
      break
    case 'LB':
      modelSocks = Socks.LB
      break
    case 'LS':
      modelSocks = Socks.LS
      break
    case 'LR':
      modelSocks = Socks.LR
      break
    case 'LY':
      modelSocks = Socks.LY
      break
    case 'LI':
      modelSocks = Socks.LI
      break
    case 'LT':
      modelSocks = Socks.LT
      break
    case 'LU':
      modelSocks = Socks.LU
      break
    case 'MO':
      modelSocks = Socks.MO
      break
    case 'MK':
      modelSocks = Socks.MK
      break
    case 'MG':
      modelSocks = Socks.MG
      break
    case 'MW':
      modelSocks = Socks.MW
      break
    case 'MY':
      modelSocks = Socks.MY
      break
    case 'MV':
      modelSocks = Socks.MV
      break
    case 'ML':
      modelSocks = Socks.ML
      break
    case 'MT':
      modelSocks = Socks.MT
      break
    case 'MH':
      modelSocks = Socks.MH
      break
    case 'MR':
      modelSocks = Socks.MR
      break
    case 'MU':
      modelSocks = Socks.MU
      break
    case 'YT':
      modelSocks = Socks.YT
      break
    case 'MX':
      modelSocks = Socks.MX
      break
    case 'FM':
      modelSocks = Socks.FM
      break
    case 'MD':
      modelSocks = Socks.MD
      break
    case 'MC':
      modelSocks = Socks.MC
      break
    case 'MN':
      modelSocks = Socks.MN
      break
    case 'ME':
      modelSocks = Socks.ME
      break
    case 'MS':
      modelSocks = Socks.MS
      break
    case 'MA':
      modelSocks = Socks.MA
      break
    case 'MZ':
      modelSocks = Socks.MZ
      break
    case 'MM':
      modelSocks = Socks.MM
      break
    case 'NA':
      modelSocks = Socks.NA
      break
    case 'NR':
      modelSocks = Socks.NR
      break
    case 'NP':
      modelSocks = Socks.NP
      break
    case 'NL':
      modelSocks = Socks.NL
      break
    case 'AN':
      modelSocks = Socks.AN
      break
    case 'NC':
      modelSocks = Socks.NC
      break
    case 'NZ':
      modelSocks = Socks.NZ
      break
    case 'NI':
      modelSocks = Socks.NI
      break
    case 'NE':
      modelSocks = Socks.NE
      break
    case 'NG':
      modelSocks = Socks.NG
      break
    case 'NU':
      modelSocks = Socks.NU
      break
    case 'KP':
      modelSocks = Socks.KP
      break
    case 'MP':
      modelSocks = Socks.MP
      break
    case 'NO':
      modelSocks = Socks.NO
      break
    case 'OM':
      modelSocks = Socks.OM
      break
    case 'PK':
      modelSocks = Socks.PK
      break
    case 'PW':
      modelSocks = Socks.PW
      break
    case 'PS':
      modelSocks = Socks.PS
      break
    case 'PA':
      modelSocks = Socks.PA
      break
    case 'PG':
      modelSocks = Socks.PG
      break
    case 'PY':
      modelSocks = Socks.PY
      break
    case 'PE':
      modelSocks = Socks.PE
      break
    case 'PH':
      modelSocks = Socks.PH
      break
    case 'PN':
      modelSocks = Socks.PN
      break
    case 'PL':
      modelSocks = Socks.PL
      break
    case 'PT':
      modelSocks = Socks.PT
      break
    case 'PR':
      modelSocks = Socks.PR
      break
    case 'QA':
      modelSocks = Socks.QA
      break
    case 'CG':
      modelSocks = Socks.CG
      break
    case 'RE':
      modelSocks = Socks.RE
      break
    case 'RO':
      modelSocks = Socks.RO
      break
    case 'RU':
      modelSocks = Socks.RU
      break
    case 'RW':
      modelSocks = Socks.RW
      break
    case 'BL':
      modelSocks = Socks.BL
      break
    case 'SH':
      modelSocks = Socks.SH
      break
    case 'KN':
      modelSocks = Socks.KN
      break
    case 'LC':
      modelSocks = Socks.LC
      break
    case 'MF':
      modelSocks = Socks.MF
      break
    case 'PM':
      modelSocks = Socks.PM
      break
    case 'VC':
      modelSocks = Socks.VC
      break
    case 'WS':
      modelSocks = Socks.WS
      break
    case 'SM':
      modelSocks = Socks.SM
      break
    case 'ST':
      modelSocks = Socks.ST
      break
    case 'SA':
      modelSocks = Socks.SA
      break
    case 'SN':
      modelSocks = Socks.SN
      break
    case 'RS':
      modelSocks = Socks.RS
      break
    case 'SC':
      modelSocks = Socks.SC
      break
    case 'SL':
      modelSocks = Socks.SL
      break
    case 'SG':
      modelSocks = Socks.SG
      break
    case 'SX':
      modelSocks = Socks.SX
      break
    case 'SK':
      modelSocks = Socks.SK
      break
    case 'SI':
      modelSocks = Socks.SI
      break
    case 'SB':
      modelSocks = Socks.SB
      break
    case 'SO':
      modelSocks = Socks.SO
      break
    case 'ZA':
      modelSocks = Socks.ZA
      break
    case 'KR':
      modelSocks = Socks.KR
      break
    case 'SS':
      modelSocks = Socks.SS
      break
    case 'ES':
      modelSocks = Socks.ES
      break
    case 'LK':
      modelSocks = Socks.LK
      break
    case 'SD':
      modelSocks = Socks.SD
      break
    case 'SR':
      modelSocks = Socks.SR
      break
    case 'SJ':
      modelSocks = Socks.SJ
      break
    case 'SZ':
      modelSocks = Socks.SZ
      break
    case 'SE':
      modelSocks = Socks.SE
      break
    case 'CH':
      modelSocks = Socks.CH
      break
    case 'SY':
      modelSocks = Socks.SY
      break
    case 'TW':
      modelSocks = Socks.TW
      break
    case 'TJ':
      modelSocks = Socks.TJ
      break
    case 'TZ':
      modelSocks = Socks.TZ
      break
    case 'TH':
      modelSocks = Socks.TH
      break
    case 'TG':
      modelSocks = Socks.TG
      break
    case 'TK':
      modelSocks = Socks.TK
      break
    case 'TO':
      modelSocks = Socks.TO
      break
    case 'TT':
      modelSocks = Socks.TT
      break
    case 'TN':
      modelSocks = Socks.TN
      break
    case 'TR':
      modelSocks = Socks.TR
      break
    case 'TM':
      modelSocks = Socks.TM
      break
    case 'TC':
      modelSocks = Socks.TC
      break
    case 'TV':
      modelSocks = Socks.TV
      break
    case 'VI':
      modelSocks = Socks.VI
      break
    case 'UG':
      modelSocks = Socks.UG
      break
    case 'UA':
      modelSocks = Socks.UA
      break
    case 'AE':
      modelSocks = Socks.AE
      break
    case 'GB':
      modelSocks = Socks.GB
      break
    case 'US':
      modelSocks = Socks.US
      break
    case 'UY':
      modelSocks = Socks.UY
      break
    case 'UZ':
      modelSocks = Socks.UZ
      break
    case 'VU':
      modelSocks = Socks.VU
      break
    case 'VA':
      modelSocks = Socks.VA
      break
    case 'VE':
      modelSocks = Socks.VE
      break
    case 'VN':
      modelSocks = Socks.VN
      break
    case 'WF':
      modelSocks = Socks.WF
      break
    case 'EH':
      modelSocks = Socks.EH
      break
    case 'YE':
      modelSocks = Socks.YE
      break
    case 'ZM':
      modelSocks = Socks.ZM
      break
    case 'ZW':
      modelSocks = Socks.ZW
      break
    default:
      modelSocks = Socks.UKNW
  }
  return modelSocks
}

module.exports = router
