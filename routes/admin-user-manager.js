var config = require('../config/database'), // get db config file
    jwt = require('jwt-simple'),
    moment = require('moment'),
    User = require('../app/models/user'), // get the mongoose model
    express = require('express'),
    router = express.Router(),
    moment = require("moment");

// route to a restricted info (GET http://localhost:8080/api/memberinfo)
router.get('/', function(req, res, next) {
    var token = req.cookies.jwt

    if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
        return res.redirect('./login');
    }

    var decoded = jwt.decode(token, config.secret);
    User.findOne({
        name: decoded.name,
        isAdmin: true
    }, function(err, user) {
        if (err) throw err;

        if (!user) {
            res.redirect('./login');
        } else {
            User.find({
                isAdmin: false
            }, "-isAdmin -_id", function(err, results) {
                if (err) {
                    return res.render('index', { title: 'Dashboard', version: config.version, time: moment().format("YYYY-MM-DD ~ h:mm:ss A") });
                } else {
                    console.log(results);
                    return res.render('user', { title: 'User Manager', version: config.version, time: moment().format("YYYY-MM-DD ~ h:mm:ss A"), moment: moment, rows: results })
                }
            });
        }
    });
});

module.exports = router;