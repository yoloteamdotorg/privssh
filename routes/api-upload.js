var User = require('../app/models/user')
var Socks = require('../app/models/socks')
var DbStats = require('../app/models/dbstats')

var config = require('../config/database')
var formidable = require('formidable')
var maxmind = require('maxmind')
var moment = require('moment')
var express = require('express')
var router = express.Router()

var jwt = require('jwt-simple')
var path = require('path')
var Q = require('q')

router.post('/UploadSimple', function (req, res) {
  var token = req.cookies.jwt

  if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
    return res.redirect('/login')
  }

  var decoded = jwt.decode(token, config.secret)
  User.findOne({
    name: decoded.name,
    isAdmin: true
  }, function (err, user) {
    if (err) throw err

    if (!user) {
      res.redirect('/login')
    } else {
      var form = new formidable.IncomingForm()

      form.uploadDir = path.join(__dirname, '../uploads')
      form.encoding = 'utf-8'
      var socksCreatedOn = moment().format('YYYY-MM-DD')

      form.parse(req, function (err, fields, files) {
        if (typeof files.fileToUpload === 'undefined') {
          return res.json({
            sucess: false,
            msg: 'No file uploaded.'
          })
        }

        var time = new Date()

        uploadSimple(socksCreatedOn, err, fields, files, res).then(function (returnStrjson) {
          returnStrjson = JSON.parse(returnStrjson)

          setTimeout(function () {
            uploadSocksDbStats(returnStrjson.uploaded || 0, files.fileToUpload.path)

            console.log(JSON.stringify({
              success: returnStrjson.success,
              hasLimit: returnStrjson.hasLimit,
              msg: returnStrjson.msg,
              err: returnStrjson.err,
              _debug: returnStrjson._debug,
              time: new Date() - time
            }))

            res.json({
              success: returnStrjson.success,
              hasLimit: returnStrjson.hasLimit,
              msg: returnStrjson.msg,
              _debug: returnStrjson._debug
            })
          }, 100)
        })
      })
    }
  })
})

router.post('/UploadSmartCountry', function (req, res) {
  var token = req.cookies.jwt

  if (!token || token.split('.').length !== 3 || !token.length || !token.trim()) {
    return res.redirect('/login')
  }

  var decoded = jwt.decode(token, config.secret)

  User.findOne({
    name: decoded.name,
    isAdmin: true
  }, function (err, user) {
    if (err) throw err

    if (!user) {
      res.redirect('/login')
    } else {
      var form = new formidable.IncomingForm()

      form.uploadDir = path.join(__dirname, '../uploads')
      form.encoding = 'utf-8'
      var socksCreatedOn = moment().format('YYYY-MM-DD')

      form.parse(req, function (err, fields, files) {
        if (typeof files.fileToUpload === 'undefined') {
          return res.json({
            sucess: false,
            msg: 'No file uploaded.'
          })
        }

        var time = new Date()

        uploadSmartCountry(socksCreatedOn, err, fields, files, res).then(function (returnStrjson) {
          returnStrjson = JSON.parse(returnStrjson)

          setTimeout(function () {
            uploadSocksDbStats(returnStrjson.uploaded || 0, files.fileToUpload.path)

            console.log(JSON.stringify({
              success: returnStrjson.success,
              hasLimit: returnStrjson.hasLimit,
              msg: returnStrjson.msg,
              _debug: returnStrjson._debug,
              time: new Date() - time
            }))

            res.json({
              success: returnStrjson.success,
              hasLimit: returnStrjson.hasLimit,
              msg: returnStrjson.msg,
              _debug: returnStrjson._debug
            })
          }, 100)
        })
      })
    }
  })
})

router.post('/autoUpload', function (req, res) {
  if (!req.headers.authorization) {
    return res.json({
      sucess: false,
      msg: 'Missing data. :/'
    })
  }

  User.findOne({
    upload_apikey: req.headers.authorization,
    isAdmin: true
  }, function (err, user) {
    if (err) throw err

    if (!user) {
      return res.status(403).send({
        success: false,
        msg: 'Authentication failed. API Key not found.'
      })
    } else {
      var form = new formidable.IncomingForm()

      form.uploadDir = path.join(__dirname, '../uploads')
      form.encoding = 'utf-8'
      var socksCreatedOn = moment().format('YYYY-MM-DD')

      form.parse(req, function (err, fields, files) {
        if (typeof files.fileToUpload === 'undefined') {
          return res.json({
            sucess: false,
            msg: 'No file uploaded.'
          })
        }

        var time = new Date()

        uploadSmartCountry(socksCreatedOn, err, fields, files, res).then(function (returnStrjson) {
          returnStrjson = JSON.parse(returnStrjson)

          setTimeout(function () {
            uploadSocksDbStats(returnStrjson.uploaded || 0, files.fileToUpload.path)

            console.log(JSON.stringify({
              success: returnStrjson.success,
              hasLimit: returnStrjson.hasLimit,
              msg: returnStrjson.msg,
              _debug: returnStrjson._debug,
              time: new Date() - time
            }))

            res.json({
              success: returnStrjson.success,
              hasLimit: returnStrjson.hasLimit,
              msg: returnStrjson.msg,
              _debug: returnStrjson._debug
            })
          }, 100)
        })
      })
    }
  })
})

var uploadSimple = function (socksCreatedOn, err, fields, files, res) {
  var items
  var Socks = getModelSocksByCountry(fields.country)
  var _counter = 0
  var _success = 0
  var _duplicate = 0
  var _err = 0
  var returnStr
  var deferred = Q.defer()
  var LineByLineReader = require('line-by-line')
  var lr = new LineByLineReader(files.fileToUpload.path, {
    encoding: 'utf8',
    skipEmptyLines: true
  })

  lr.on('error', function () {
    returnStr = JSON.stringify({
      success: false,
      hasLimit: false,
      msg: 'Socks Upload Error!',
      _debug: "Can't Read Upload file(s)"
    })

    deferred.resolve(returnStr)
  })

  lr.on('line', function (line) {
    items = line.split(fields.split || '|')

    if (items.length < 3) {
      _err++
      lr.resume()
    } else {
      var _socksIp = items[0].replace(/\s/g, '')
      var _socksUsr = items[1].replace(/\s/g, '')
      var _socksPwd = items[2].replace(/\s/g, '')

      lr.pause()

      _counter++

      if (_counter > 5000) {
        lr.close()

        setTimeout(function () {
          returnStr = JSON.stringify({
            success: true,
            hasLimit: true,
            msg: 'Done! Uploaded: ' + _success + '/5000 (Duplicate: ' + _duplicate + ' - Error: ' + _err + ')',
            uploaded: _success
          })

          deferred.resolve(returnStr)
        }, 1000)
      } else {
        if (maxmind.validate(items[0]) !== false) {
          if (fields.duplicateRemoval === 'on') {
            Socks.findBySsh(_socksIp, _socksUsr, _socksPwd, function (err, listSocks) {
              if (err) throw err

              if (!listSocks) {
                let newSock = Socks({
                  ip: _socksIp,
                  user: _socksUsr,
                  pass: _socksPwd,
                  state: items[fields.state - 1] || items[4] || 'UnknowState',
                  city: items[fields.city - 1] || items[5] || 'UnknowCity',
                  createtOn: socksCreatedOn
                })

                newSock.save(function (err) {
                  if (err) throw err
                  _success++
                })
              } else {
                _duplicate++
              }
            }).then(function () {
              lr.resume()
            })
          } else {
            let newSock = Socks({
              ip: _socksIp,
              user: _socksUsr,
              pass: _socksPwd,
              state: items[fields.state - 1] || items[4] || 'UnknowState',
              city: items[fields.city - 1] || items[5] || 'UnknowCity',
              createtOn: socksCreatedOn
            })

            newSock.save(function (err) {
              if (err) throw err
              _success++
            }).then(function () {
              lr.resume()
            })
          }
        } else {
          _err++
          lr.resume()
        }
      }
    }
  })

  lr.on('end', function () {
    setTimeout(function () {
      if (_counter === 0) {
        returnStr = JSON.stringify({
          success: false,
          hasLimit: false,
          msg: 'Error: file format invalid!',
          _debug: 'File format invalid!'
        })
      } else {
        returnStr = JSON.stringify({
          success: true,
          hasLimit: false,
          msg: 'Done! Uploaded: ' + _success + '/' + _counter + ' (Duplicate: ' + _duplicate + ' - Error: ' + _err + ')',
          uploaded: _success
        })
      }

      deferred.resolve(returnStr)
    }, 500)
  })

  return deferred.promise
}

var uploadSmartCountry = function (socksCreatedOn, err, fields, files, res) {
  var _counter = 0
  var _success = 0
  var _duplicate = 0
  var _err = 0
  var items

  var returnStr
  var deferred = Q.defer()
  var LineByLineReader = require('line-by-line')
  var lr = new LineByLineReader(files.fileToUpload.path, {
    encoding: 'utf8',
    skipEmptyLines: true
  })

  lr.on('error', function () {
    returnStr = JSON.stringify({
      success: false,
      hasLimit: false,
      msg: 'Socks Upload Error!',
      _debug: "Can't Read Upload file(s)"
    })

    deferred.resolve(returnStr)
  })

  lr.on('line', function (line) {
    items = line.split(fields.split || '|')

    lr.pause()
    _counter++

    if (items.length < 3) {
      _err++
      lr.resume()
    } else {
      var _socksIp = items[0].replace(/\s/g, '')
      var _socksUsr = items[1].replace(/\s/g, '')
      var _socksPwd = items[2].replace(/\s/g, '')

      if (_counter > 5000) {
        lr.close()

        setTimeout(function () {
          returnStr = JSON.stringify({
            success: true,
            hasLimit: true,
            msg: 'Done! Uploaded: ' + _success + '/5000 (Duplicate: ' + _duplicate + ' - Error: ' + _err + ')',
            uploaded: _success
          })

          deferred.resolve(returnStr)
        }, 1000)
      } else {
        maxmindGeoLite2(_socksIp).then(function (country) {
          try {
            if (country !== -1) {
              var modelCountry = getModelSocksByCountry(country)

              if (fields.duplicateRemoval === 'on') {
                modelCountry.findBySsh(_socksIp, _socksUsr, _socksPwd, function (err, listSocks) {
                  if (err) throw err

                  if (!listSocks) {
                    let newSock = modelCountry({
                      ip: _socksIp,
                      user: _socksUsr,
                      pass: _socksPwd,
                      state: items[fields.state - 1] || items[4] || 'UnknowState',
                      city: items[fields.city - 1] || items[5] || 'UnknowCity',
                      createtOn: socksCreatedOn
                    })

                    newSock.save(function (err) {
                      if (err) throw err
                      _success++
                    })
                  } else {
                    _duplicate++
                  }
                })
              } else {
                let newSock = modelCountry({
                  ip: _socksIp,
                  user: _socksUsr,
                  pass: _socksPwd,
                  state: items[fields.state - 1] || items[4] || 'UnknowState',
                  city: items[fields.city - 1] || items[5] || 'UnknowCity',
                  createtOn: socksCreatedOn
                })

                newSock.save(function (err) {
                  if (err) throw err
                  _success++
                })
              }
            } else {
              throw new Error('WrongIP')
            }
          } catch (err) {
            _err++
            console.log(err)
          } finally {
            lr.resume()
          }
        })
      }
    }
  })

  lr.on('end', function () {
    setTimeout(function () {
      if (_counter === 0) {
        returnStr = JSON.stringify({
          success: false,
          hasLimit: false,
          msg: 'Error: file format invalid!',
          _debug: 'File format invalid!'
        })
      } else {
        returnStr = JSON.stringify({
          success: true,
          hasLimit: false,
          msg: 'Done! Uploaded: ' + _success + '/' + _counter + ' (Duplicate: ' + _duplicate + ' - Error: ' + _err + ')',
          uploaded: _success
        })
      }

      deferred.resolve(returnStr)
    }, 500)
  })

  return deferred.promise
}

var maxmindGeoLite2 = function (ip) {
  var deferred = Q.defer()

  if (maxmind.validate(ip) === false) {
    console.log('%s not valid', ip)
    deferred.resolve(-1)
  }

  maxmind.open('./db/geolite2-country.mmdb', function (err, geoLite2) {
    if (err) throw err

    var country = 'UKNW'
    var countryLookup

    try {
      countryLookup = geoLite2.get(ip)
      if (countryLookup !== null) {
        country = (typeof countryLookup.country === 'undefined') ? 'UKNW' : countryLookup.country.iso_code
      }
    } catch (e) {
      console.log(e)
    } finally {
      deferred.resolve(country)
    }
  })

  return deferred.promise
}

var uploadSocksDbStats = function (socks, filePath) {
  console.log('uploaded:', socks)

  filePath = filePath || false

  DbStats.findOne({
    rootStats: true
  }, function (err, stats) {
    if (err) throw err

    if (moment().diff(stats.lastUpdate, 'days') > 0) {
      console.log('diff > 0')
      stats.update({
        uploaded: socks,
        lastUpdate: moment().format('YYYY-MM-DD')
      }, {
        new: true,
        upsert: true
      },
        function (err, results) {
          if (err) throw err
          console.log(results)
        })
    } else {
      console.log('diff= 0')
      stats.update({
        $inc: {
          uploaded: socks
        },
        lastUpdate: moment().format('YYYY-MM-DD')
      }, {
        new: true,
        upsert: true
      },
        function (err, results) {
          if (err) throw err
          console.log(results)
        })
    }
  })

  if (filePath !== false) {
    var fs = require('fs')
    fs.unlink(filePath, function () {
      console.log('File deleted: ' + filePath)
    })
  }
}

var getModelSocksByCountry = function (country) {
  var modelSocks
  switch (country.toUpperCase()) {
    case 'AF':
      modelSocks = Socks.AF
      break
    case 'AL':
      modelSocks = Socks.AL
      break
    case 'DZ':
      modelSocks = Socks.DZ
      break
    case 'AS':
      modelSocks = Socks.AS
      break
    case 'AD':
      modelSocks = Socks.AD
      break
    case 'AO':
      modelSocks = Socks.AO
      break
    case 'AI':
      modelSocks = Socks.AI
      break
    case 'AQ':
      modelSocks = Socks.AQ
      break
    case 'AG':
      modelSocks = Socks.AG
      break
    case 'AR':
      modelSocks = Socks.AR
      break
    case 'AM':
      modelSocks = Socks.AM
      break
    case 'AW':
      modelSocks = Socks.AW
      break
    case 'AU':
      modelSocks = Socks.AU
      break
    case 'AT':
      modelSocks = Socks.AT
      break
    case 'AZ':
      modelSocks = Socks.AZ
      break
    case 'BS':
      modelSocks = Socks.BS
      break
    case 'BH':
      modelSocks = Socks.BH
      break
    case 'BD':
      modelSocks = Socks.BD
      break
    case 'BB':
      modelSocks = Socks.BB
      break
    case 'BY':
      modelSocks = Socks.BY
      break
    case 'BE':
      modelSocks = Socks.BE
      break
    case 'BZ':
      modelSocks = Socks.BZ
      break
    case 'BJ':
      modelSocks = Socks.BJ
      break
    case 'BM':
      modelSocks = Socks.BM
      break
    case 'BT':
      modelSocks = Socks.BT
      break
    case 'BO':
      modelSocks = Socks.BO
      break
    case 'BA':
      modelSocks = Socks.BA
      break
    case 'BW':
      modelSocks = Socks.BW
      break
    case 'BR':
      modelSocks = Socks.BR
      break
    case 'IO':
      modelSocks = Socks.IO
      break
    case 'VG':
      modelSocks = Socks.VG
      break
    case 'BN':
      modelSocks = Socks.BN
      break
    case 'BG':
      modelSocks = Socks.BG
      break
    case 'BF':
      modelSocks = Socks.BF
      break
    case 'BI':
      modelSocks = Socks.BI
      break
    case 'KH':
      modelSocks = Socks.KH
      break
    case 'CM':
      modelSocks = Socks.CM
      break
    case 'CA':
      modelSocks = Socks.CA
      break
    case 'CV':
      modelSocks = Socks.CV
      break
    case 'KY':
      modelSocks = Socks.KY
      break
    case 'CF':
      modelSocks = Socks.CF
      break
    case 'TD':
      modelSocks = Socks.TD
      break
    case 'CL':
      modelSocks = Socks.CL
      break
    case 'CN':
      modelSocks = Socks.CN
      break
    case 'CX':
      modelSocks = Socks.CX
      break
    case 'CC':
      modelSocks = Socks.CC
      break
    case 'CO':
      modelSocks = Socks.CO
      break
    case 'KM':
      modelSocks = Socks.KM
      break
    case 'CK':
      modelSocks = Socks.CK
      break
    case 'CR':
      modelSocks = Socks.CR
      break
    case 'HR':
      modelSocks = Socks.HR
      break
    case 'CU':
      modelSocks = Socks.CU
      break
    case 'CW':
      modelSocks = Socks.CW
      break
    case 'CY':
      modelSocks = Socks.CY
      break
    case 'CZ':
      modelSocks = Socks.CZ
      break
    case 'CD':
      modelSocks = Socks.CD
      break
    case 'DK':
      modelSocks = Socks.DK
      break
    case 'DJ':
      modelSocks = Socks.DJ
      break
    case 'DM':
      modelSocks = Socks.DM
      break
    case 'DO':
      modelSocks = Socks.DO
      break
    case 'TL':
      modelSocks = Socks.TL
      break
    case 'EC':
      modelSocks = Socks.EC
      break
    case 'EG':
      modelSocks = Socks.EG
      break
    case 'SV':
      modelSocks = Socks.SV
      break
    case 'GQ':
      modelSocks = Socks.GQ
      break
    case 'ER':
      modelSocks = Socks.ER
      break
    case 'EE':
      modelSocks = Socks.EE
      break
    case 'ET':
      modelSocks = Socks.ET
      break
    case 'FK':
      modelSocks = Socks.FK
      break
    case 'FO':
      modelSocks = Socks.FO
      break
    case 'FJ':
      modelSocks = Socks.FJ
      break
    case 'FI':
      modelSocks = Socks.FI
      break
    case 'FR':
      modelSocks = Socks.FR
      break
    case 'PF':
      modelSocks = Socks.PF
      break
    case 'GA':
      modelSocks = Socks.GA
      break
    case 'GM':
      modelSocks = Socks.GM
      break
    case 'GE':
      modelSocks = Socks.GE
      break
    case 'DE':
      modelSocks = Socks.DE
      break
    case 'GH':
      modelSocks = Socks.GH
      break
    case 'GI':
      modelSocks = Socks.GI
      break
    case 'GR':
      modelSocks = Socks.GR
      break
    case 'GL':
      modelSocks = Socks.GL
      break
    case 'GD':
      modelSocks = Socks.GD
      break
    case 'GU':
      modelSocks = Socks.GU
      break
    case 'GT':
      modelSocks = Socks.GT
      break
    case 'GG':
      modelSocks = Socks.GG
      break
    case 'GN':
      modelSocks = Socks.GN
      break
    case 'GW':
      modelSocks = Socks.GW
      break
    case 'GY':
      modelSocks = Socks.GY
      break
    case 'HT':
      modelSocks = Socks.HT
      break
    case 'HN':
      modelSocks = Socks.HN
      break
    case 'HK':
      modelSocks = Socks.HK
      break
    case 'HU':
      modelSocks = Socks.HU
      break
    case 'IS':
      modelSocks = Socks.IS
      break
    case 'IN':
      modelSocks = Socks.IN
      break
    case 'ID':
      modelSocks = Socks.ID
      break
    case 'IR':
      modelSocks = Socks.IR
      break
    case 'IQ':
      modelSocks = Socks.IQ
      break
    case 'IE':
      modelSocks = Socks.IE
      break
    case 'IM':
      modelSocks = Socks.IM
      break
    case 'IL':
      modelSocks = Socks.IL
      break
    case 'IT':
      modelSocks = Socks.IT
      break
    case 'CI':
      modelSocks = Socks.CI
      break
    case 'JM':
      modelSocks = Socks.JM
      break
    case 'JP':
      modelSocks = Socks.JP
      break
    case 'JE':
      modelSocks = Socks.JE
      break
    case 'JO':
      modelSocks = Socks.JO
      break
    case 'KZ':
      modelSocks = Socks.KZ
      break
    case 'KE':
      modelSocks = Socks.KE
      break
    case 'KI':
      modelSocks = Socks.KI
      break
    case 'XK':
      modelSocks = Socks.XK
      break
    case 'KW':
      modelSocks = Socks.KW
      break
    case 'KG':
      modelSocks = Socks.KG
      break
    case 'LA':
      modelSocks = Socks.LA
      break
    case 'LV':
      modelSocks = Socks.LV
      break
    case 'LB':
      modelSocks = Socks.LB
      break
    case 'LS':
      modelSocks = Socks.LS
      break
    case 'LR':
      modelSocks = Socks.LR
      break
    case 'LY':
      modelSocks = Socks.LY
      break
    case 'LI':
      modelSocks = Socks.LI
      break
    case 'LT':
      modelSocks = Socks.LT
      break
    case 'LU':
      modelSocks = Socks.LU
      break
    case 'MO':
      modelSocks = Socks.MO
      break
    case 'MK':
      modelSocks = Socks.MK
      break
    case 'MG':
      modelSocks = Socks.MG
      break
    case 'MW':
      modelSocks = Socks.MW
      break
    case 'MY':
      modelSocks = Socks.MY
      break
    case 'MV':
      modelSocks = Socks.MV
      break
    case 'ML':
      modelSocks = Socks.ML
      break
    case 'MT':
      modelSocks = Socks.MT
      break
    case 'MH':
      modelSocks = Socks.MH
      break
    case 'MR':
      modelSocks = Socks.MR
      break
    case 'MU':
      modelSocks = Socks.MU
      break
    case 'YT':
      modelSocks = Socks.YT
      break
    case 'MX':
      modelSocks = Socks.MX
      break
    case 'FM':
      modelSocks = Socks.FM
      break
    case 'MD':
      modelSocks = Socks.MD
      break
    case 'MC':
      modelSocks = Socks.MC
      break
    case 'MN':
      modelSocks = Socks.MN
      break
    case 'ME':
      modelSocks = Socks.ME
      break
    case 'MS':
      modelSocks = Socks.MS
      break
    case 'MA':
      modelSocks = Socks.MA
      break
    case 'MZ':
      modelSocks = Socks.MZ
      break
    case 'MM':
      modelSocks = Socks.MM
      break
    case 'NA':
      modelSocks = Socks.NA
      break
    case 'NR':
      modelSocks = Socks.NR
      break
    case 'NP':
      modelSocks = Socks.NP
      break
    case 'NL':
      modelSocks = Socks.NL
      break
    case 'AN':
      modelSocks = Socks.AN
      break
    case 'NC':
      modelSocks = Socks.NC
      break
    case 'NZ':
      modelSocks = Socks.NZ
      break
    case 'NI':
      modelSocks = Socks.NI
      break
    case 'NE':
      modelSocks = Socks.NE
      break
    case 'NG':
      modelSocks = Socks.NG
      break
    case 'NU':
      modelSocks = Socks.NU
      break
    case 'KP':
      modelSocks = Socks.KP
      break
    case 'MP':
      modelSocks = Socks.MP
      break
    case 'NO':
      modelSocks = Socks.NO
      break
    case 'OM':
      modelSocks = Socks.OM
      break
    case 'PK':
      modelSocks = Socks.PK
      break
    case 'PW':
      modelSocks = Socks.PW
      break
    case 'PS':
      modelSocks = Socks.PS
      break
    case 'PA':
      modelSocks = Socks.PA
      break
    case 'PG':
      modelSocks = Socks.PG
      break
    case 'PY':
      modelSocks = Socks.PY
      break
    case 'PE':
      modelSocks = Socks.PE
      break
    case 'PH':
      modelSocks = Socks.PH
      break
    case 'PN':
      modelSocks = Socks.PN
      break
    case 'PL':
      modelSocks = Socks.PL
      break
    case 'PT':
      modelSocks = Socks.PT
      break
    case 'PR':
      modelSocks = Socks.PR
      break
    case 'QA':
      modelSocks = Socks.QA
      break
    case 'CG':
      modelSocks = Socks.CG
      break
    case 'RE':
      modelSocks = Socks.RE
      break
    case 'RO':
      modelSocks = Socks.RO
      break
    case 'RU':
      modelSocks = Socks.RU
      break
    case 'RW':
      modelSocks = Socks.RW
      break
    case 'BL':
      modelSocks = Socks.BL
      break
    case 'SH':
      modelSocks = Socks.SH
      break
    case 'KN':
      modelSocks = Socks.KN
      break
    case 'LC':
      modelSocks = Socks.LC
      break
    case 'MF':
      modelSocks = Socks.MF
      break
    case 'PM':
      modelSocks = Socks.PM
      break
    case 'VC':
      modelSocks = Socks.VC
      break
    case 'WS':
      modelSocks = Socks.WS
      break
    case 'SM':
      modelSocks = Socks.SM
      break
    case 'ST':
      modelSocks = Socks.ST
      break
    case 'SA':
      modelSocks = Socks.SA
      break
    case 'SN':
      modelSocks = Socks.SN
      break
    case 'RS':
      modelSocks = Socks.RS
      break
    case 'SC':
      modelSocks = Socks.SC
      break
    case 'SL':
      modelSocks = Socks.SL
      break
    case 'SG':
      modelSocks = Socks.SG
      break
    case 'SX':
      modelSocks = Socks.SX
      break
    case 'SK':
      modelSocks = Socks.SK
      break
    case 'SI':
      modelSocks = Socks.SI
      break
    case 'SB':
      modelSocks = Socks.SB
      break
    case 'SO':
      modelSocks = Socks.SO
      break
    case 'ZA':
      modelSocks = Socks.ZA
      break
    case 'KR':
      modelSocks = Socks.KR
      break
    case 'SS':
      modelSocks = Socks.SS
      break
    case 'ES':
      modelSocks = Socks.ES
      break
    case 'LK':
      modelSocks = Socks.LK
      break
    case 'SD':
      modelSocks = Socks.SD
      break
    case 'SR':
      modelSocks = Socks.SR
      break
    case 'SJ':
      modelSocks = Socks.SJ
      break
    case 'SZ':
      modelSocks = Socks.SZ
      break
    case 'SE':
      modelSocks = Socks.SE
      break
    case 'CH':
      modelSocks = Socks.CH
      break
    case 'SY':
      modelSocks = Socks.SY
      break
    case 'TW':
      modelSocks = Socks.TW
      break
    case 'TJ':
      modelSocks = Socks.TJ
      break
    case 'TZ':
      modelSocks = Socks.TZ
      break
    case 'TH':
      modelSocks = Socks.TH
      break
    case 'TG':
      modelSocks = Socks.TG
      break
    case 'TK':
      modelSocks = Socks.TK
      break
    case 'TO':
      modelSocks = Socks.TO
      break
    case 'TT':
      modelSocks = Socks.TT
      break
    case 'TN':
      modelSocks = Socks.TN
      break
    case 'TR':
      modelSocks = Socks.TR
      break
    case 'TM':
      modelSocks = Socks.TM
      break
    case 'TC':
      modelSocks = Socks.TC
      break
    case 'TV':
      modelSocks = Socks.TV
      break
    case 'VI':
      modelSocks = Socks.VI
      break
    case 'UG':
      modelSocks = Socks.UG
      break
    case 'UA':
      modelSocks = Socks.UA
      break
    case 'AE':
      modelSocks = Socks.AE
      break
    case 'GB':
      modelSocks = Socks.GB
      break
    case 'US':
      modelSocks = Socks.US
      break
    case 'UY':
      modelSocks = Socks.UY
      break
    case 'UZ':
      modelSocks = Socks.UZ
      break
    case 'VU':
      modelSocks = Socks.VU
      break
    case 'VA':
      modelSocks = Socks.VA
      break
    case 'VE':
      modelSocks = Socks.VE
      break
    case 'VN':
      modelSocks = Socks.VN
      break
    case 'WF':
      modelSocks = Socks.WF
      break
    case 'EH':
      modelSocks = Socks.EH
      break
    case 'YE':
      modelSocks = Socks.YE
      break
    case 'ZM':
      modelSocks = Socks.ZM
      break
    case 'ZW':
      modelSocks = Socks.ZW
      break
    default:
      modelSocks = Socks.UKNW
  }
  return modelSocks
}

module.exports = router

/*
function upload_smart_city () {
  var time = new Date()

  _saveToDB().then(function (returnStrjson) {
    returnStrjson = JSON.parse(returnStrjson)

    setTimeout(function () {
      // uploadSocksDbStats(returnStrjson.uploaded || 0, files.fileToUpload.path);
      console.log(JSON.stringify({
        success: returnStrjson.success,
        hasLimit: returnStrjson.hasLimit,
        msg: returnStrjson.msg,
        _debug: returnStrjson._debug,
        time: new Date() - time
      }))
    }, 100)
  })

  function _saveToDB () {
    var counter = 0,
      success = 1,
      duplicate = 0,
      err = 0,
      returnStr

    var Socks2

    var deferred = Q.defer()

    var LineByLineReader = require('line-by-line'),
      lr = new LineByLineReader(path.join(__dirname, '../uploads/1'), {
        encoding: 'utf8',
        skipEmptyLines: true
      })

    lr.on('error', function (err) {
      console.log('lr.on_error')
      returnStr = JSON.stringify({
        success: false,
        hasLimit: false,
        msg: 'Socks Upload Error!',
        _debug: "Can't Read Upload file(s)"
      })

      deferred.resolve(returnStr)
    })

    lr.on('line', function (line) {
      items = line.split('|')

      lr.pause()

      counter++

      console.log(counter)

      if (counter === 5000) {
        lr.close()

        returnStr = JSON.stringify({
          success: true,
          hasLimit: true,
          msg: 'Done! Uploaded: ' + success + '/' + counter + ' (Duplicate: ' + duplicate + ')',
          uploaded: success
        })

        deferred.resolve(returnStr)
      } else {
        dsadsa().then(function (geoInfo) {
          /*
                              Socks2.findBySsh(items[0], items[1], items[2], function(err, listSocks) {
                                  if (err) {
                                      throw err;
                                  } else {
                                      if (!listSocks) {
                                          var newSock = Socks({
                                              ip: items[0],
                                              user: items[1],
                                              pass: items[2],
                                              state: geoInfo.state,
                                              city: geoInfo.city,
                                              createtOn: socksCreatedOn
                                          });

                                          newSock.save(function(err) {
                                              if (err) {
                                                  throw err;
                                              }
                                              success++;
                                              console.log("succcc")
                                          });
                                      } else {
                                          duplicate++;
                                      }
                                  }
                              }).then(function() {
                                  *-/
          lr.resume()
          // });
        })

        function dsadsa () {
          var geoInfo
          var deferred2 = Q.defer()
          var cityLookup = maxmind.openSync('./db/GeoIP2-City.mmdb', {
            cache: {
              max: 1000, // max items in cache
              maxAge: 10000 // life time in milliseconds
            }
          })

          try {
            var GeoIP2 = cityLookup.get(items[0])

            geoInfo = JSON.stringify({
              country: (typeof GeoIP2.country === 'undefined') ? 'UKNW' : GeoIP2.country.iso_code,
              state: (typeof GeoIP2.subdivisions === 'undefined') ? 'UnknowSate' : GeoIP2.subdivisions[0]['names'].en,
              city: (typeof GeoIP2.city === 'undefined') ? 'UnknowCity' : GeoIP2.city.names.en
            })

            // console.log(items)
            // console.log("%j", geoInfo)
          } catch (e) {
            console.log(e)
            geoInfo = JSON.stringify({
              country: 'UKNW',
              state: 'UnknowSate',
              city: 'UnknowCity'
            })
          } finally {
            // Socks2 = getModelSocksByCountry(GeoInfo.country);
            setTimeout(function () {
              deferred2.resolve(JSON.parse(geoInfo))
            }, 100)
          }

          return deferred2.promise
        }
      }
    })

    lr.on('end', function () {
      returnStr = JSON.stringify({
        success: true,
        hasLimit: false,
        msg: 'Done! Uploaded: ' + success + '/' + counter + ' (Duplicate: ' + duplicate + ')',
        uploaded: success
      })
      deferred.resolve(returnStr)
    })

    return deferred.promise
  }
}
*/
