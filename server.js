// PrivateSshManager by YoloTEAM
// Author: James - wormiit
// NodeJS is awesome!

// Load modules
var express = require('express')
var path = require('path')
var app = express()
var favicon = require('serve-favicon')
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
var morgan = require('morgan')
var mongoose = require('mongoose')
var DbStats = require('./app/models/dbstats')

var server = require('http').createServer()
var io = require('socket.io')(server)

if (!process.env.OPENSHIFT_NODEJS_PORT) {
  mongoose.Promise = global.Promise
}

var passport = require('passport')
var config = require('./config/database') // get db config file
var serverPort = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080
var serverIpAddress = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'

app.set('views', path.join(__dirname) + '/views')
app.set('view engine', 'pug')

app.use(express.static(path.join(__dirname) + '/public'))
app.use(bodyParser.json())
app.use(cookieParser())
app.use(morgan('dev')) // log to console
app.use(passport.initialize()) // Use the passport package in our application
app.use(favicon(path.join(__dirname) + '/public/img/fav.ico'))

// get our request parameters
app.use(bodyParser.urlencoded({
  extended: false
}))

// Create Upload folder if not exists
var fs = require('fs')
fs.existsSync('./uploads') || fs.mkdirSync('./uploads')

// webinterface routing
var adminDashboard = require('./routes/admin-dashboard')
var adminLogin = require('./routes/admin-login')
var adminUser = require('./routes/admin-user-manager')
var adminSocks = require('./routes/admin-socks-manager')
var adminCampaign = require('./routes/admin-campaign')
var adminAPI = require('./routes/admin-apikey')

// api routing
var apiAdminUser = require('./routes/api-admin-user')
var apiAdminSocks = require('./routes/api-admin-socks')
var apiAdminCampaign = require('./routes/api-admin-campaign')
var apiMemberLogin = require('./routes/api-member-login')
var apiMemberInfo = require('./routes/api-member-info')
var apiSocks = require('./routes/api-socks')
var apiUpload = require('./routes/api-upload')

// connect the routes for web interface
app.use('/', adminDashboard)
app.use('/login', adminLogin)
app.use('/user', adminUser)
app.use('/socks', adminSocks)
app.use('/campaign', adminCampaign)
app.use('/api', adminAPI)

app.get('/v3', function (req, res) {
  res.render('v3')
})

app.get('/clientchanger', function (req, res) {
  res.render('client')
})

app.get('/logout', function (req, res) {
  res.clearCookie('jwt')
  res.redirect('.')
})

// connect the api routes under /api/*
app.use('/api/admin-user', apiAdminUser)
app.use('/api/admin-socks', apiAdminSocks)
app.use('/api/admin-campaign', apiAdminCampaign)

app.use('/api/getSocks', apiSocks)
app.use('/api/uploadSocks', apiUpload)

app.use('/api/member/login', apiMemberLogin)
app.use('/api/member/info', apiMemberInfo)

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// ----- error handlers
// development error handler
// will print stacktrace
// if (app.get('env') === 'development') {
//   app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.json({ error: err })
//   });
// }

// production error handler
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//   res.status(err.status || 500);
//   res.json({ error: err })
// });
// -----

// Connect to MongoDb
mongoose.connect(config.database, function (err) {
  if (err) {
    console.log('Mongo Connection Error', err.message)
  } else {
    console.log('Mongo Connection Successful')
    DbStats.findOneAndUpdate({rootStats: true}, {}, {upsert: true}, function (err, doc) {
      if (err) throw err
      console.log(doc)
    })
  }
})

// Start the server
app.listen(serverPort, serverIpAddress, function () {
  console.log('Listening on port: http://' + serverIpAddress + ':' + serverPort)
})

io.on('connection', function (client) {
  console.log('new user connection: ', client.id)

  client.on('disconnect', function () {
    console.log('user disconnected')
  })

  client.on('control', function (data) {
    console.log(data)
    io.send(data.command)
    io.emit('control', JSON.stringify(data))
  })
})

server.listen(serverPort + 1, serverIpAddress, function () {
  console.log('listening on : ', serverPort + 1)
})
